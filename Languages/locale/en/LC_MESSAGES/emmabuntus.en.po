# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the emmabuntus package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: emmabuntus\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-02-14 00:02+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: Yves Saboret <yves.saboret@gmail.com>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#, sh-format
msgid "Welcome to "
msgstr ""

#, sh-format
msgid "This distribution is made to refurbish computers,"
msgstr ""

#, sh-format
msgid "and let everyone discover GNU/Linux."
msgstr ""

#, sh-format
msgid "Username:"
msgstr ""

#, sh-format
msgid "the user and root passwords are both:"
msgstr ""

#, sh-format
msgid "Emmabuntüs - Welcome"
msgstr ""

#, sh-format
msgid "Tutorials           "
msgstr ""

#, sh-format
msgid "Debian Handbook "
msgstr ""

#, sh-format
msgid "Debian Reference"
msgstr ""

#, sh-format
msgid "Tools                  "
msgstr ""

#, sh-format
msgid "Settings Manager "
msgstr ""

#, sh-format
msgid "Software Center  "
msgstr ""

#, sh-format
msgid "Forum            "
msgstr ""

#, sh-format
msgid "Videos            "
msgstr ""

#, sh-format
msgid "Contribute       "
msgstr ""

#, sh-format
msgid "Show this window at next startup            "
msgstr ""

#, sh-format
msgid "Close"
msgstr ""

#, sh-format
msgid "Would you like to install non-free software on this system ?"
msgstr ""

#, sh-format
msgid "Note:"
msgstr ""

#, sh-format
msgid "You will be able to install these non-free software later on, by going to"
msgstr ""

#, sh-format
msgid "the application Menu (icon at the top left), then \\042Non-Free Software\\042."
msgstr ""

#, sh-format
msgid "Warning:"
msgstr ""

#, sh-format
msgid "The packages \\042Proprietary codecs\\042 you are about to install are not supported"
msgstr ""

#, sh-format
msgid "by the Ubuntu or Debian developers communities, and might be illegal in some countries."
msgstr ""

#, sh-format
msgid "The main reason why these \\042Proprietary codecs\\042 are not included by default is a matter of law."
msgstr ""

#, sh-format
msgid "It imposes a fee for each player using these \\042Proprietary codecs\\042,"
msgstr ""

#, sh-format
msgid "before installing them, your are responsible to check their usage restrictions in your country."
msgstr ""

#, sh-format
msgid "Allows you to play protected DVDs"
msgstr ""

#, sh-format
msgid "Codecs for audio only"
msgstr ""

#, sh-format
msgid "Allows you to play audio in MP3 format"
msgstr ""

#, sh-format
msgid "Non-free software installation"
msgstr ""

#, sh-format
msgid "Allows you to watch videos stored in the Flash format used by video-on-demand platforms"
msgstr ""

#, sh-format
msgid "Adobe Flash Player Plugin for Firefox: You need an internet connection"
msgstr ""

#, sh-format
msgid "Skype is proprietary software that allows users to make phone or video calls over Internet, as well as screen sharing"
msgstr ""

#, sh-format
msgid "Skype (as free alternatives, we strongly suggest you use Jitsi or Jitsi Meet, which are included in Emmabuntüs)"
msgstr ""

#, sh-format
msgid "TeamViewer is a proprietary software application for remote control, desktop sharing, online meetings, web conferencing and file transfer between computers."
msgstr ""

#, sh-format
msgid "Allows better compatibility with documents produced by Microsoft Office suite up to the 2003 version"
msgstr ""

#, sh-format
msgid "Arial Microsoft Fonts"
msgstr ""

#, sh-format
msgid "Allows better compatibility with documents produced by Microsoft Office suite since the 2007 version"
msgstr ""

#, sh-format
msgid "Calibri Microsoft Fonts"
msgstr ""

#, sh-format
msgid "Show this window at next startup"
msgstr ""

#, sh-format
msgid "Repository for"
msgstr ""

#, sh-format
msgid "Repository Update"
msgstr ""

#, sh-format
msgid "Installation"
msgstr ""

#, sh-format
msgid "Waiting for the configuration"
msgstr ""

#, sh-format
msgid "is already installed"
msgstr ""

#, sh-format
msgid "was already installed via internet"
msgstr ""

#, sh-format
msgid "Installation in progress"
msgstr ""

#, sh-format
msgid "Initialization ..."
msgstr ""

#, sh-format
msgid "Installation canceled."
msgstr ""

#, sh-format
msgid "Install"
msgstr ""

#, sh-format
msgid "You wish to install"
msgstr ""

#, sh-format
msgid "Two installation modes are available : the Classic mode for a personal install "
msgstr ""

#, sh-format
msgid "or the OEM mode dedicated to the systems integrators."
msgstr ""

#, sh-format
msgid "in OEM mode, the user name is:"
msgstr ""

#, sh-format
msgid "Cancel"
msgstr ""

#, sh-format
msgid "OEM installation  "
msgstr ""

#, sh-format
msgid "Classic installation  "
msgstr ""

#, sh-format
msgid "You are about to finalize the OEM installation before shipping it to the final user(s)."
msgstr ""

#, sh-format
msgid "After this window activation, you won\\047t be able to use the OEM environment at the next startup."
msgstr ""

#, sh-format
msgid "if you wish to define the current OEM user environment as the one by default"
msgstr ""

#, sh-format
msgid "for the final users, please select either the first or both options below:"
msgstr ""

#, sh-format
msgid "Finalize the OEM installation"
msgstr ""

#, sh-format
msgid "Please enter a unique name for this batch of systems using only alphanumeric characters. This name will be saved on the installed system on the file"
msgstr ""

#, sh-format
msgid "If you copy only the OEM environment, the final user account creation will launch the post-installation process, allowing the user to define it\\047s own configuration (defining dock usage level, enabling applications like RedShifts, etc...)"
msgstr ""

#, sh-format
msgid "Copy the OEM environment as reference for the final user"
msgstr ""

#, sh-format
msgid "By copying also the OEM post-install configuration, this step will be skipped after the account creation of the final user, who will then inherits of the entire OEM configuration"
msgstr ""

#, sh-format
msgid "Copy the OEM post-install configuration for the final user(s)"
msgstr ""

#, sh-format
msgid "Finalize"
msgstr ""

#, sh-format
msgid "The computer is ready for shipment"
msgstr ""

#, sh-format
msgid "and it will stop."
msgstr ""

#, sh-format
msgid "Copying in progress"
msgstr ""

#, sh-format
msgid "Copy ..."
msgstr ""

#, sh-format
msgid "Copy canceled."
msgstr ""

#, sh-format
msgid "Currently your keyboard map is:"
msgstr ""

#, sh-format
msgid "Keyboard configuration"
msgstr ""

#, sh-format
msgid " Switch "
msgstr ""

#, sh-format
msgid " Keep "
msgstr ""

#, sh-format
msgid "Welcome on"
msgstr ""

#, sh-format
msgid "Thanks for trying our distribution. We hope it will meet your expectations."
msgstr ""

#, sh-format
msgid "With old or resource limited computers, we suggest you use"
msgstr ""

#, sh-format
msgid "the LXQt desktop environment which is lighter than XFCE, the one installed by default."
msgstr ""

#, sh-format
msgid "During login, you can select your environment, at any time,"
msgstr ""

#, sh-format
msgid "by clicking on the gear icon (at the top right of the screen)."
msgstr ""

#, sh-format
msgid "If you want to use LXQt right now, select the left button."
msgstr ""

#, sh-format
msgid "Thanks for having installed our distribution. We hope it will meet your expectations."
msgstr ""

#, sh-format
msgid "Several post-installation windows are going to pop-up."
msgstr ""

#, sh-format
msgid "They allow you to configure your brand-new system."
msgstr ""

#, sh-format
msgid "Installation configuration for  "
msgstr ""

#, sh-format
msgid "Switch to LXQt  "
msgstr ""

#, sh-format
msgid "Continue under XFCE  "
msgstr ""

#, sh-format
msgid "You will be redirected to the login screen."
msgstr ""

#, sh-format
msgid "Please enter your password to switch to LXQt."
msgstr ""

#, sh-format
msgid "Enabling Composite"
msgstr ""

#, sh-format
msgid "Creation of the desktop shortcuts"
msgstr ""

#, sh-format
msgid "Do you want to access to Free Culture documents on your USB drive?"
msgstr ""

#, sh-format
msgid "Do you want to install the Free Culture documents from your USB drive to your hard drive ?"
msgstr ""

#, sh-format
msgid "Changing the wallpaper"
msgstr ""

#, sh-format
msgid "Desktop configuration (continued)"
msgstr ""

#, sh-format
msgid "Desktop configuration"
msgstr ""

#, sh-format
msgid "Configuration (Application Launcher):"
msgstr ""

#, sh-format
msgid "Information:"
msgstr ""

#, sh-format
msgid "During training sessions or public presentations,     "
msgstr ""

#, sh-format
msgid "we recommend you lock the dock."
msgstr ""

#, sh-format
msgid "You can change these settings later on, by going to:  "
msgstr ""

#, sh-format
msgid "Application menu -> Cairo-Dock."
msgstr ""

#, sh-format
msgid "Panel"
msgstr ""

#, sh-format
msgid "Dashboard configuration"
msgstr ""

#, sh-format
msgid "Application menu -> Settings -> Emmabuntüs - Desktop."
msgstr ""

#, sh-format
msgid "System configuration:"
msgstr ""

#, sh-format
msgid "Activate the dock (Application Launcher at the bottom of the screen), allowing beginners to use the computer more easily"
msgstr ""

#, sh-format
msgid "Locks the dock to prevent unwanted changes"
msgstr ""

#, sh-format
msgid "The taskbar is located in the banner at the top of the screen, and allows you to quickly switch between applications"
msgstr ""

#, sh-format
msgid "The workspace selector is located in the banner at the top of the screen, and allows you to quickly switch between desktops"
msgstr ""

#, sh-format
msgid "Eye protection against blue light. You can go to the"
msgstr ""

#, sh-format
msgid "file to set different color temperatures between day and night"
msgstr ""

#, sh-format
msgid "Allows to have a clipboard manager accessible in the system tray"
msgstr ""

#, sh-format
msgid "The dark theme is less tiring for your eyes"
msgstr ""

#, sh-format
msgid "Accessibility enhanced theme with 3 pixel window borders"
msgstr ""

#, sh-format
msgid "Allows you to have sound notifications when emails arrive, tasks are completed, etc."
msgstr ""

#, sh-format
msgid "Activate the dock"
msgstr ""

#, sh-format
msgid "Lock the Emmabuntüs default dock"
msgstr ""

#, sh-format
msgid "Activate the taskbar"
msgstr ""

#, sh-format
msgid "Activate the workspace selector"
msgstr ""

#, sh-format
msgid "Activate RedShift"
msgstr ""

#, sh-format
msgid "Activate the clipboard manager"
msgstr ""

#, sh-format
msgid "Activate a dark theme"
msgstr ""

#, sh-format
msgid "Activate sound events"
msgstr ""

#, sh-format
msgid "Please select the application menu version that best suits your needs: "
msgstr ""

#, sh-format
msgid "Classic menu is more suited for older computers. The new Whisker menu is more ergonomic, but requires more RAM."
msgstr ""

#, sh-format
msgid "Classic"
msgstr ""

#, sh-format
msgid "Whisker"
msgstr ""

#, sh-format
msgid "Please select the screen saver of your choice: "
msgstr ""

#, sh-format
msgid "Images of the Abbé Pierre Foundation campaign against inadequate housing or from Ubuntu wallpaper set."
msgstr ""

#, sh-format
msgid "Please select the wallpaper of your choice: "
msgstr ""

#, sh-format
msgid "Free wallpapers taken from: L.L. de Mars, Odysseus Libre."
msgstr ""

#, sh-format
msgid "Enable automatic launch of Bluetooth"
msgstr ""

#, sh-format
msgid "Start gSpeech (Text to Speech) at system startup"
msgstr ""

#, sh-format
msgid "Enable automatic login"
msgstr ""

#, sh-format
msgid "Do you want to create the package cache?"
msgstr ""

#, sh-format
msgid "Your system has no package cache."
msgstr ""

#, sh-format
msgid "This prevents you from installing new software,"
msgstr ""

#, sh-format
msgid "or to perform software updates."
msgstr ""

#, sh-format
msgid "Package cache usage fixed"
msgstr ""

#, sh-format
msgid "Operation in progress"
msgstr ""

#, sh-format
msgid "Creation ..."
msgstr ""

#, sh-format
msgid "Your computer has a Swap partition"
msgstr ""

#, sh-format
msgid "and it is not used by your system."
msgstr ""

#, sh-format
msgid "Do you want to activate it automatically at system startup?"
msgstr ""

#, sh-format
msgid "Do you want to fix this and use this Swap partition automatically at every boot?"
msgstr ""

#, sh-format
msgid "Fix Swap usage"
msgstr ""

#, sh-format
msgid "Installation Failed."
msgstr ""

#, sh-format
msgid "User directories protection in Emmabuntüs prevents"
msgstr ""

#, sh-format
msgid "using Samba in guest mode for this directory"
msgstr ""

#, sh-format
msgid "If you want to use Samba in guest mode, you must:"
msgstr ""

#, sh-format
msgid "Create a folder directly under / home"
msgstr ""

#, sh-format
msgid "Give all rights to this folder"
msgstr ""

#, sh-format
msgid "Add the rights in the Samba configuration"
msgstr ""

#, sh-format
msgid "see below:"
msgstr ""

#, sh-format
msgid "Would you like to automatically configure the network share in guest mode below?"
msgstr ""

#, sh-format
msgid "The name of the folder is empty!"
msgstr ""

#, sh-format
msgid "The network share already exists in the Samba configuration!"
msgstr ""

#, sh-format
msgid "Information on Samba usage"
msgstr ""

#, sh-format
msgid "Sharing in"
msgstr ""

#, sh-format
msgid "Enable write share"
msgstr ""

#, sh-format
msgid "Show this window at next launch"
msgstr ""

#, sh-format
msgid "Emmabuntüs - Tools"
msgstr ""

#, sh-format
msgid "These applications help you manage your system."
msgstr ""

#, sh-format
msgid "Live"
msgstr ""

#, sh-format
msgid "Maintenance"
msgstr ""

#, sh-format
msgid "Configuration"
msgstr ""

#, sh-format
msgid "Printing"
msgstr ""

#, sh-format
msgid "Utilities"
msgstr ""

#, sh-format
msgid "Do you want to fix the picture tearing problem of your Intel/AMD graphics card? "
msgstr ""

#, sh-format
msgid "After applying this hotfix, you need to sign out or restart your computer."
msgstr ""

#, sh-format
msgid "This patch is only available for Intel/AMD graphics cards."
msgstr ""

#, sh-format
msgid "This fix is already applied.                        "
msgstr ""

#, sh-format
msgid "Image tearing fixed"
msgstr ""

#, sh-format
msgid "Do you want to reinstall the Emmabuntus default dock (Cairo-Dock) ?"
msgstr ""

#, sh-format
msgid "Any changes or customization of the dock will be lost"
msgstr ""

#, sh-format
msgid "after the launch of this utility. The dock will return to its initial state defined during system install."
msgstr ""

#, sh-format
msgid "Reset Emmabuntus dock"
msgstr ""

#, sh-format
msgid "In the context of education or public presentation usage"
msgstr ""

#, sh-format
msgid "we recommend the dock lock activation."
msgstr ""

#, sh-format
msgid "Want to lock the Emmabuntüs default dock (Cairo-Dock) ?"
msgstr ""

#, sh-format
msgid "Want to unlock the Emmabuntüs default dock (Cairo-Dock) ?"
msgstr ""

#, sh-format
msgid "Lock/Unlock dock"
msgstr ""

#, sh-format
msgid "Do you want to enable the Emmabuntüs default dock (Cairo-Dock) ?     "
msgstr ""

#, sh-format
msgid "Do you want to disable the Emmabuntüs default dock (Cairo-Dock) ?     "
msgstr ""

#, sh-format
msgid "All          :"
msgstr ""

#, sh-format
msgid "Simple   :"
msgstr ""

#, sh-format
msgid "Basic     :"
msgstr ""

#, sh-format
msgid "For those wanting access to all applications"
msgstr ""

#, sh-format
msgid "For newbies and my dear old aunt ;-)"
msgstr ""

#, sh-format
msgid "For children and very beginners"
msgstr ""

#, sh-format
msgid "You are using a version of Cairo-Dock which is not the Emmabuntüs default one."
msgstr ""

#, sh-format
msgid "Do you want to replace it by the Emmabuntüs default version?"
msgstr ""

#, sh-format
msgid "This will delete your version of the Cairo-Dock."
msgstr ""

#, sh-format
msgid "Do you want to mask this information window at next startup?"
msgstr ""

#, sh-format
msgid "which is not the one used by default for the next boot emmabuntüs?     "
msgstr ""

#, sh-format
msgid "Choose which dock version"
msgstr ""

#, sh-format
msgid "Please select the dock version which best fits your needs:"
msgstr ""

#, sh-format
msgid "Do you want to remove unnecessary languages in the distribution ?"
msgstr ""

#, sh-format
msgid "Some languages are not needed by your system"
msgstr ""

#, sh-format
msgid "removing them will spare some disk space,"
msgstr ""

#, sh-format
msgid "and also prevent downloading updates for these languages."
msgstr ""

#, sh-format
msgid "Please select the languages you no longer need:"
msgstr ""

#, sh-format
msgid "Language Management"
msgstr ""

#, sh-format
msgid "Install additional language packs for the selected language in the drop-down menu below. Requires an active Internet connection."
msgstr ""

#, sh-format
msgid "Install the complements of the selected language below:"
msgstr ""

#, sh-format
msgid "Requires a computer restart to take the new language into account"
msgstr ""

#, sh-format
msgid "Set the language selected above as the default system language"
msgstr ""

#, sh-format
msgid "Removing French language"
msgstr ""

#, sh-format
msgid "Removing English language"
msgstr ""

#, sh-format
msgid "Removing Spanish language"
msgstr ""

#, sh-format
msgid "Removing Italian language"
msgstr ""

#, sh-format
msgid "Removing German language"
msgstr ""

#, sh-format
msgid "Removing Portuguese language"
msgstr ""

#, sh-format
msgid "Removing Danish language"
msgstr ""

#, sh-format
msgid "Removing Arabic language"
msgstr ""

#, sh-format
msgid "Installing additional language packages for the system current language"
msgstr ""

#, sh-format
msgid "To make the default language change effective, you must restart your computer."
msgstr ""

#, sh-format
msgid "Do you want to restart your computer now?"
msgstr ""

#, sh-format
msgid "Removing ..."
msgstr ""

#, sh-format
msgid "Operation canceled."
msgstr ""

#, sh-format
msgid "Changing the default language"
msgstr ""

#, sh-format
msgid "Do you want to launch gSpeech (Text to Speech) at system startup?"
msgstr ""

#, sh-format
msgid "This application is also accessible via the dock, and the drop down menu at the top left."
msgstr ""

#, sh-format
msgid "We advise against using gSpeech on computers with less than 512MB of RAM     "
msgstr ""

#, sh-format
msgid "in order to minimize the memory usage."
msgstr ""

#, sh-format
msgid "If you want later to turn this feature on at every boot,"
msgstr ""

#, sh-format
msgid "please run in a Terminal the command: init_autostart_gspeech"
msgstr ""

#, sh-format
msgid "Select the non-free software you want to install ?"
msgstr ""

#, sh-format
msgid "Choice"
msgstr ""

#, sh-format
msgid "Names of software or library"
msgstr ""

#, sh-format
msgid "Packages \\042Proprietary codecs\\042 you are about to install are not supported"
msgstr ""

#, sh-format
msgid "(by the Debian or Ubuntu developer communities), and may be illegal in some countries."
msgstr ""

#, sh-format
msgid "If you\\047re in a country that requires software patents (that is to say neither Belgium nor France nor Switzerland ...),     "
msgstr ""

#, sh-format
msgid "however the law allows you to buy addons such as"
msgstr ""

#, sh-format
msgid "instead of the automatic installation of these \\042Proprietary codecs\\042."
msgstr ""

#, sh-format
msgid "The main reason why these \\042Proprietary codecs\\042 are not included is just a matter of law."
msgstr ""

#, sh-format
msgid "it is your responsibility to check their usage restrictions in your country before installation."
msgstr ""

#, sh-format
msgid "Please enter your administrator password:"
msgstr ""

#, sh-format
msgid "Would you like to install non-free software"
msgstr ""

#, sh-format
msgid "on this system ?"
msgstr ""

#, sh-format
msgid "This non-free software is available in the distribution, to facilitate its access by eveyone,    "
msgstr ""

#, sh-format
msgid "pending the arrival of alternatives, so that newbies can quickly start."
msgstr ""

#, sh-format
msgid "We strongly encourage you to use a free alternative."
msgstr ""

#, sh-format
msgid "Non-free software"
msgstr ""

#, sh-format
msgid "is already installed, do you want to reinstall it?"
msgstr ""

#, sh-format
msgid "This non-free software is available in the distribution, to facilitate its access by eveyone,     "
msgstr ""

#, sh-format
msgid "This non-free software is available in the distribution to facilitate the communication with everybody.    "
msgstr ""

#, sh-format
msgid "As free alternatives, we strongly suggest you use Jitsi or"
msgstr ""

#, sh-format
msgid "which are already included in Emmabuntüs.     "
msgstr ""

#, sh-format
msgid "You need an active internet connection to install this software.   "
msgstr ""

#, sh-format
msgid "Some languages are not needed in your system"
msgstr ""

#, sh-format
msgid "removing them will spare you some space,"
msgstr ""

#, sh-format
msgid "and avoid future updates for these languages."
msgstr ""

#, sh-format
msgid "Removing unnecessary languages"
msgstr ""

#, sh-format
msgid "Please select the languages that you no longer need"
msgstr ""

#, sh-format
msgid "Languages installed on your system"
msgstr ""

#, sh-format
msgid "Do you want the unnecessary languages removal window to popup at next startup ?"
msgstr ""

#, sh-format
msgid "Installation canceled, you need an active internet connection.     "
msgstr ""

#, sh-format
msgid "To install the Flash Player and Microsoft fonts you need an internet connection.     "
msgstr ""

#, sh-format
msgid "Do you still want to continue other non-free software installation?"
msgstr ""

#, sh-format
msgid "You can install these software later on by going to the menu Maintenance -> Non-free software"
msgstr ""

#, sh-format
msgid "Removing"
msgstr ""

#, sh-format
msgid "thank you for waiting"
msgstr ""

#, sh-format
msgid "Adobe Flash Player Plugin for Firefox - Warning: You need an internet connection"
msgstr ""

#, sh-format
msgid "Enhanced devices (WiFi, Audio, etc.)  support with non-free drivers"
msgstr ""

#, sh-format
msgid "Skype (As free alternatives, we strongly suggest you use Jitsi or Jitsi Meet, which are already included in Emmabuntüs)"
msgstr ""

#, sh-format
msgid "Would you like to install"
msgstr ""

#, sh-format
msgid "It is not possible to install software"
msgstr ""

#, sh-format
msgid "in live mode."
msgstr ""

#, sh-format
msgid "Yes"
msgstr ""

#, sh-format
msgid "No"
msgstr ""

#, sh-format
msgid "MB"
msgstr ""

#, sh-format
msgid "GB"
msgstr ""

#, sh-format
msgid "CPU Model"
msgstr ""

#, sh-format
msgid "Wrong CPU model detected, please enter a new value :"
msgstr ""

#, sh-format
msgid "RAM capacity"
msgstr ""

#, sh-format
msgid "Wrong RAM capacity detected, please enter a new value in GB :"
msgstr ""

#, sh-format
msgid "HDD capacity"
msgstr ""

#, sh-format
msgid "Wrong HDD capacity detected, please enter a new value in GB :"
msgstr ""

#, sh-format
msgid "Machine description and sale price card"
msgstr ""

#, sh-format
msgid "Description"
msgstr ""

#, sh-format
msgid "CPU"
msgstr ""

#, sh-format
msgid "RAM"
msgstr ""

#, sh-format
msgid "Hard disk drive"
msgstr ""

#, sh-format
msgid "Sale price"
msgstr ""

#, sh-format
msgid "Comments"
msgstr ""

#, sh-format
msgid "Computer price"
msgstr ""

#, sh-format
msgid "Tower + Keyboard/Mouse"
msgstr ""

#, sh-format
msgid "Please enter the configuration of you computer for sale"
msgstr ""

#, sh-format
msgid "This sale-card was already entered, do you want to use it ?"
msgstr ""

#, sh-format
msgid "Wrong description or price !"
msgstr ""

#, sh-format
msgid "Please re-enter the sale-card data"
msgstr ""

#, sh-format
msgid "Please enter the admin password :"
msgstr ""

#, sh-format
msgid "Selection"
msgstr ""

#, sh-format
msgid "Type of hardware"
msgstr ""

#, sh-format
msgid "Tower + Screen + Keyboard/Mouse"
msgstr ""

#, sh-format
msgid "Tower"
msgstr ""

#, sh-format
msgid "Laptop with Power-Adapter"
msgstr ""

#, sh-format
msgid "Laptop without Power-Adapter"
msgstr ""

#, sh-format
msgid "Free comments field"
msgstr ""

#, sh-format
msgid "Computer description"
msgstr ""

#, sh-format
msgid "Please fill the hardware description in :"
msgstr ""

#, sh-format
msgid "Data entry area"
msgstr ""

#, sh-format
msgid "Enter the computer sale price in"
msgstr ""

#, sh-format
msgid "software installation canceled"
msgstr ""

#, sh-format
msgid "Note"
msgstr ""

#, sh-format
msgid "Please enter a note if the computer has a defect, or leave this field empty and click on \\047Validate\\047 :"
msgstr ""

#, sh-format
msgid "Error : Wrong description or price !"
msgstr ""

#, sh-format
msgid "Removing Japanese language"
msgstr ""

#, sh-format
msgid "Free solution for computer remote control"
msgstr ""

#, sh-format
msgid "Select one of the two modes of use :"
msgstr ""

#, sh-format
msgid "Authorize the control"
msgstr ""

#, sh-format
msgid "Allows another user to remotely take the control of your computer."
msgstr ""

#, sh-format
msgid "To pick this option, select the \\042Run\\042 mode in the next window, then click on \\042Next\\042."
msgstr ""

#, sh-format
msgid "Then send the connexion codes of the second window to the person wants to take the control of your computer,"
msgstr ""

#, sh-format
msgid "and leave this window open during all the time of the remote control session."
msgstr ""

#, sh-format
msgid "Control a computer"
msgstr ""

#, sh-format
msgid "Allows you to remotely control another computer."
msgstr ""

#, sh-format
msgid "To do this, go to the \\<b>DWService\\</b> internet page, enter the codes sent by the person"
msgstr ""

#, sh-format
msgid "who wants you to take control of his or of her computer,"
msgstr ""

#, sh-format
msgid "in the \\042Login\\042 frame, then click on \\042Sign in\\042."
msgstr ""

#, sh-format
msgid "Allows the security packages to be automatically updated"
msgstr ""

#, sh-format
msgid "Enable automatic security updates"
msgstr ""

#, sh-format
msgid "The default language change is not effective       "
msgstr ""

#, sh-format
msgid "Do you want to install the parental control software \\042CTparental\\042 in your computer ?"
msgstr ""

#, sh-format
msgid "If you have minor children at home, we strongly encourage you to install this software to protect children from violent, pornographic, etc. content on Internet."
msgstr ""

#, sh-format
msgid "for this protection to be effective, it is important that your children use accounts without administrator rights, which will be created automatically if the option below is checked. The child accounts created will not have any password. To create these accounts, enter the children\\047s names separated by a space in the field provided for this purpose."
msgstr ""

#, sh-format
msgid "It is important that your children never use your own account which, by default, does not have these protections for minors and allows the installation of third party software that may be prohibited to minors."
msgstr ""

#, sh-format
msgid "after its installation, CTparental allows you to also define Internet access time slots for your children via its administration interface which is located under the Applications menu (icon on the top left), then Internet section. For more information read this tutorial."
msgstr ""

#, sh-format
msgid "Automatic creation of child accounts"
msgstr ""

#, sh-format
msgid "Allows the creation of the child accounts defined below"
msgstr ""

#, sh-format
msgid "Child accounts names :"
msgstr ""

#, sh-format
msgid "Specify in this field the names of your children separated by a space"
msgstr ""

#, sh-format
msgid "Launching the configuration interface"
msgstr ""

#, sh-format
msgid "For more information on how to configure parental controls read our online tutorial"
msgstr ""

#, sh-format
msgid "girl boy"
msgstr ""

#, sh-format
msgid "CTparental software installation"
msgstr ""

#, sh-format
msgid "Child accounts password :"
msgstr ""

#, sh-format
msgid "Specify in this field the passwords of your children separated by a space. If you do not want to put passwords leave this field empty."
msgstr ""

#, sh-format
msgid "for this protection to be effective, it is important that your children use accounts without administrator rights, which will be created automatically if the option below is checked. To create these accounts, put the names of your children separated by a space in the field provided. If you want to set a password for each child account, enter the passwords separated by a space and having at least 6 characters composed of letters and numbers. Otherwise leave this field empty if you don\\047t want to use passwords."
msgstr ""

#, sh-format
msgid "Create a guest account without password that will be reset at each startup"
msgstr ""

#, sh-format
msgid "Account for use in a school, a media library, in a public place"
msgstr ""

#, sh-format
msgid "Installation was successful.\\n\\nRemember to check that the parental control is working properly on the child or guest accounts,\\nbefore letting your children use the computer.\\nFor greater security autologin was disabled on all accounts."
msgstr ""

#, sh-format
msgid "Installation failed."
msgstr ""

#, sh-format
msgid "Installation was successful."
msgstr ""

#, sh-format
msgid "In the current versions of Deb-get and Deborah, the software coming from Ubuntu\\nrepositories or PPAs cannot be installed on Emmabuntüs Debian Edition"
msgstr ""

#, sh-format
msgid "During the \\042CTparental\\042 installation, you will have to enter the account name and the password of the parental control administration interface. This password must contain at least 6 characters and one lowercase, one uppercase, one number, and one special character."
msgstr ""

#, sh-format
msgid "Tutorial"
msgstr ""

#, sh-format
msgid "Unable to install.\\n\\napt/dpkg database locked.\\nTry installing CTparental later."
msgstr ""
