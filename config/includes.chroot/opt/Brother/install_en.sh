#!/bin/bash
# Original :
#   Script d'installation pour imprimantes Brother
#   par demonipuch <demonipuch@gmail.com>
#
# This version is the English eqivalent of the Brother installation script,
# origonaly written in French by demonipuch
# (Translation by Yves Saboret from the Emmabuntüs collective)

#########################################################################
# This program is free software: you can redistribute it and/or modify  #
# it under the terms of the GNU General Public License as published by  #
# the Free Software Foundation, either version 2 of the License, or     #
# (at your option) any later version.                                   #
#                                                                       #
# This program is distributed in the hope that it will be useful,       #
# but WITHOUT ANY WARRANTY; without even the implied warranty of        #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         #
# GNU General Public License for more details.                          #
#                                                                       #
# You should have received a copy of the GNU General Public License     #
# along with this program.  If not, see <http://www.gnu.org/licenses/>. #
#########################################################################

##################
### Parameters ###
### -CLI to force the command line interpreter (ignore zenity)

#################
### VARIABLES ###
#################

os=$(lsb_release -is)
codename=$(lsb_release -cs)
pwd=$(dirname $0)
arch=$(uname -m)
who=$(who -m|awk '{print $1}')
blue="\\033[1;34m"
green="\\033[1;32m"
red="\\033[1;31m"
resetcolor="\\033[0;0m"

LongTitle="Installation script for Brother printers"
ShortTitle="Install Wizard"

#################
### FONCTIONS ###
#################

Log() {
# Create the script logfile
exec 9> ${pwd}/install.log
BASH_XTRACEFD=9
set -x
}

CheckZenity() {
# Check if zenity is available
# otherwise use whiptail

dpkg -l | awk '{print $2}' | grep ^zenity$ 1>&2>/dev/null
if [[ $? == 0 ]] && [[ ! -z $DISPLAY ]]
    then
    InstallWizard="zenity"
else
    InstallWizard="whiptail"
fi
}

GetUID() {
# display an error message if the script was not launched
# with the necessary execution rights
if [[ $UID != "0" ]]; then
    echo -e ${red}"Please relaunch this script as super-user :${resetcolor} sudo bash $0"
    exit 1
fi
}

##########################
###  CLI mode WIZARD   ###
##########################

Help() {
# Display a message in order to help the end user
# with its navigation within the whiptail window
whiptail --msgbox --backtitle="${LongTitle}" --title="Help" \
    "To navigate , you can use the Arrows and the Tab keys.\
\nTo validate a choice press the Enter key." 9 70
}

CheckSourceList() {
case ${os} in
    Debian)
        repo="main"
        os="debian"
        rules="/lib/udev/rules.d/60-libsane.rules"
    ;;
    Ubuntu)
        repo="universe"
        os="ubuntu"
        rules="/lib/udev/rules.d/40-libsane.rules"
    ;;
    LinuxMint)
        case ${codename} in
            debian)
                os=""
                repo="main"
                rules="/lib/udev/rules.d/60-libsane.rules"
            ;;
            petra)
                codename="saucy"
                os="ubuntu"
                repo="universe"
                rules="/lib/udev/rules.d/40-libsane.rules"
            ;;
            olivia)
                codename="raring"
                os="ubuntu"
                repo="universe"
                rules="/lib/udev/rules.d/40-libsane.rules"
            ;;
            nadia)
                codename="quantal"
                os="ubuntu"
                repo="universe"
                rules="/lib/udev/rules.d/40-libsane.rules"
            ;;
            maya)
                codename="precise"
                os="ubuntu"
                repo="universe"
                rules="/lib/udev/rules.d/40-libsane.rules"
            ;;
            lisa)
                codename="oneiric"
                os="ubuntu"
                repo="universe"
                rules="/lib/udev/rules.d/40-libsane.rules"
            ;;
            katya)
                codename="natty"
                os="ubuntu"
                repo="universe"
                rules="/lib/udev/rules.d/40-libsane.rules"
            ;;
            julia)
                codename="maverick"
                os="ubuntu"
                repo="universe"
                rules="/lib/udev/rules.d/40-libsane.rules"
            ;;
            isadora)
                codename="lucid"
                os="ubuntu"
                repo="universe"
                rules="/lib/udev/rules.d/40-libsane.rules"
            ;;
        esac
    ;;
    elementary\ OS)
        repo="universe"
        os="ubuntu"
        if [[ ${codename} == "luna" ]]; then
            codename="precise"
        fi
        rules="/lib/dev/rules.d/40-libsane.rules"
    ;;
esac
# Check if the repository containing the ia32-libs packet is active
if [[ ${arch} == "x86_64" || ${arch} == "amd64" ]]; then
    if [[ ! $(grep ${repo} /etc/apt/sources.list | grep -E "^deb http://(ftp.[a-z]{2}.|[a-z]{2}.|packages.|deb.|http.|httpredir.)?(archive.ubuntu.com|debian.org|linuxmint.com)/(${os})?(/)? ${codename} ") ]]; then
        whiptail --msgbox --backtitle="${LongTitle}" --title="${ShortTitle}" \
            "Please activate the ${repo} repository, then relaunch this script." \
            9 70 3>&1 1>&2 2>&3
        exit 1
    fi
fi
}

GetModel() {
# Display a list of the printer families
model=$(whiptail --menu --backtitle="${LongTitle}" --title="${ShortTitle}" \
    "\nSelect your printer family :" \
    13 70 4 "DCP" "" "HL" "" "FAX" "" "MFC" "" \
    3>&1 1>&2 2>&3)
# Quit the script if no selection
if [[ -z ${model} ]]; then
    exit 1
fi
}

GetPrinter() {
# Display a list of the printer models
printer=$(whiptail --menu --backtitle="${LongTitle}" --title="${ShortTitle}" \
    "\nSelect your printer model :" \
    15 70 6 $(grep "${model}-" ${pwd}/url_printer_drivers|sed 's/$/ \r/g;s/#//g') \
    3>&1 1>&2 2>&3)
# Quit the script if no selection
if [[ -z ${printer} ]]; then
    exit 1
fi
}

GetConnectionType() {
# Ask for the type of connection (USB or Network)
connection=$(whiptail --menu --backtitle="${LongTitle}" --title="${ShortTitle}" \
        "\nConnection type of your printer :" \
        11 70 2 "USB" "" "Network" "" \
        3>&1 1>&2 2>&3)
# Quit the script immediately if no selection
if [[ -z ${connection} ]]; then
    exit 1
fi
}

GetIpAddress() {
# Ask for printer IP address
ip=$(whiptail --inputbox --backtitle="${LongTitle}" --title="${ShortTitle}" \
        "\nEnter the IP address of your printer :" \
        10 70 3>&1 1>&2 2>&3)
if [[ -z ${ip} ]]; then
    exit 1
fi
# Execute a ping test
tmp="/tmp/.brother"
for i in 0 25 50 75 100; do
    echo $i
    # if tezst is ok, create a temp file
    if ping -w1 ${ip} > /dev/null; then
        touch ${tmp}
    fi

done | whiptail --gauge --backtitle="${LongTitle}" --title="${ShortTitle}" \
            "\nChecking connection with device at ${ip}" \
            9 70 0 3>&1 1>&2 2>&3
# Display a message depending on the test result
if [[ -e ${tmp} ]]; then
    whiptail --msgbox --backtitle="${LongTitle}" --title="${ShortTitle}" \
            "Connection test successful." \
            9 70 0 3>&1 1>&2 2>&3
    rm ${tmp}
else
    whiptail --msgbox --backtitle="${LongTitle}" --title="${ShortTitle}" \
            "The connection test failed. \
\nPlease verify that the IP address is correct. \
/NThe install wizard will abort now." \
            9 70 0 3>&1 1>&2 2>&3
    exit 1
fi
}

ScanKeyToolsInstall() {
# Ask if user wants to install the Scan-Key packet
whiptail --yesno --backtitle="${LongTitle}" --title="${ShortTitle}" \
            "Do you want to install the Scan-Key tools ?" \
            9 70 3>&1 1>&2 2>&3
if [[ $? == "0" ]]; then
    keytools="4"
else
    keytools="2"
fi
}

ConfirmInstall() {
# Double check that everything is OK
# before starting the actual installation
case ${connection} in
    # Message for USB type of install
    USB)
    confirm=$(whiptail --yesno --backtitle="${LongTitle}" --title="${ShortTitle}" \
            "Please confim the installation of your  ${printer} printer \
\nconnected with a USB cable ?" \
            9 70 3>&1 1>&2 2>&3)
    ;;
    # Message for Network type of install
    Network)
    confirm=$(whiptail --yesno --backtitle="${LongTitle}" --title="${ShortTitle}" \
            "Please confim the installation of your  ${printer} printer \
\nconnected to the network at the IP address ${ip}?" \
            9 70 3>&1 1>&2 2>&3)
    ;;
esac
# Quit the script immediately if no selection
if [[ $? == 1 ]]; then
    exit 1
fi
}

#############################
### Wizard in GUI mode    ###
#############################

CheckSourceList_GUI() {
case ${os} in
    Debian)
        repo="main"
        os="debian"
        rules="/lib/udev/rules.d/60-libsane.rules"
    ;;
    Ubuntu)
        repo="universe"
        os="ubuntu"
        rules="/lib/udev/rules.d/40-libsane.rules"
    ;;
    LinuxMint)
        case ${codename} in
            debian)
                os=""
                repo="main"
                rules="/lib/udev/rules.d/60-libsane.rules"
            ;;
            petra)
                codename="saucy"
                os="ubuntu"
                repo="universe"
                rules="/lib/udev/rules.d/40-libsane.rules"
            ;;
            olivia)
                codename="raring"
                os="ubuntu"
                repo="universe"
                rules="/lib/udev/rules.d/40-libsane.rules"
            ;;
            nadia)
                codename="quantal"
                os="ubuntu"
                repo="universe"
                rules="/lib/udev/rules.d/40-libsane.rules"
            ;;
            maya)
                codename="precise"
                os="ubuntu"
                repo="universe"
                rules="/lib/udev/rules.d/40-libsane.rules"
            ;;
            lisa)
                codename="oneiric"
                os="ubuntu"
                repo="universe"
                rules="/lib/udev/rules.d/40-libsane.rules"
            ;;
            katya)
                codename="natty"
                os="ubuntu"
                repo="universe"
                rules="/lib/udev/rules.d/40-libsane.rules"
            ;;
            julia)
                codename="maverick"
                os="ubuntu"
                repo="universe"
                rules="/lib/udev/rules.d/40-libsane.rules"
            ;;
            isadora)
                codename="lucid"
                os="ubuntu"
                repo="universe"
                rules="/lib/udev/rules.d/40-libsane.rules"
            ;;
        esac
    ;;
    "elementary OS")
        repo="universe"
        os="ubuntu"
        if [[ ${codename} == "luna" ]]; then
            codename="precise"
        fi
        rules="/lib/dev/rules.d/40-libsane.rules"
    ;;
esac
# Display an error message if the Universe repository is not activated
# only for Ubuntu 64 bits (ia32-libs)
if [[ ${arch} == "x86_64" || ${arch} == "amd64" ]]; then
    if [[ ! $(grep ${repo} /etc/apt/sources.list | grep -E "^deb http://(ftp.[a-z]{2}.|[a-z]{2}.|packages.|deb.|http.|httpredir.)?(archive.ubuntu.com|debian.org|linuxmint.com)/(${os})?(/)? ${codename} ") ]]; then
        zenity --error --width=400 \
            --title="${LongTitle}" \
            --text="Please activate the ${repo} repository, then relaunch the script."
        exit 1
    fi
fi
}

GetModel_GUI() {
# Display the list of printer families.
model=$(zenity --list --radiolist --width=400 --height=240 \
            --title="${LongTitle}" \
            --text="Select your printer family :\n" --column="" --column="" \
FALSE "DCP" \
FALSE "FAX" \
FALSE "HL" \
FALSE "MFC")
# Quitter si aucune sélection
if [[ -z ${model} ]]; then
    exit 1
fi
}

GetPrinter_GUI() {
# Display printer models for model DCP
if [[ ${model} == "DCP" ]]; then
printer=$(zenity --list --radiolist --width=400 --height=420 \
            --title="${LongTitle}" \
            --text="Select your printer model :\n" --column="" --column="" \
FALSE "DCP-1000" \
FALSE "DCP-110C" \
FALSE "DCP-115C" \
FALSE "DCP-117C" \
FALSE "DCP-120C" \
FALSE "DCP-130C" \
FALSE "DCP-135C" \
FALSE "DCP-1400" \
FALSE "DCP-145C" \
FALSE "DCP-150C" \
FALSE "DCP-1518" \
FALSE "DCP-153C" \
FALSE "DCP-155C" \
FALSE "DCP-1612W" \
FALSE "DCP-163C" \
FALSE "DCP-165C" \
FALSE "DCP-167C" \
FALSE "DCP-185C" \
FALSE "DCP-195C" \
FALSE "DCP-197C" \
FALSE "DCP-310CN" \
FALSE "DCP-315CN" \
FALSE "DCP-330C" \
FALSE "DCP-340CW" \
FALSE "DCP-350C" \
FALSE "DCP-353C" \
FALSE "DCP-357C" \
FALSE "DCP-365CN" \
FALSE "DCP-373CW" \
FALSE "DCP-375CW" \
FALSE "DCP-377CW" \
FALSE "DCP-383C" \
FALSE "DCP-385C" \
FALSE "DCP-387C" \
FALSE "DCP-395CN" \
FALSE "DCP-540CN" \
FALSE "DCP-560CN" \
FALSE "DCP-585CW" \
FALSE "DCP-6690CW" \
FALSE "DCP-7010" \
FALSE "DCP-7020" \
FALSE "DCP-7025" \
FALSE "DCP-7030" \
FALSE "DCP-7040" \
FALSE "DCP-7045N" \
FALSE "DCP-7055" \
FALSE "DCP-7055W" \
FALSE "DCP-7057" \
FALSE "DCP-7057WR" \
FALSE "DCP-7060D" \
FALSE "DCP-7065DN" \
FALSE "DCP-7070DW" \
FALSE "DCP-750CW" \
FALSE "DCP-770CW" \
FALSE "DCP-8020" \
FALSE "DCP-8025D" \
FALSE "DCP-8040" \
FALSE "DCP-8045D" \
FALSE "DCP-8060" \
FALSE "DCP-8065DN" \
FALSE "DCP-8070D" \
FALSE "DCP-8080DN" \
FALSE "DCP-8085DN" \
FALSE "DCP-8110D" \
FALSE "DCP-8110DN" \
FALSE "DCP-8112DN" \
FALSE "DCP-8150DN" \
FALSE "DCP-8152DN" \
FALSE "DCP-8155DN" \
FALSE "DCP-8157DN" \
FALSE "DCP-8250DN" \
FALSE "DCP-9010CN" \
FALSE "DCP-9040CN" \
FALSE "DCP-9042CDN" \
FALSE "DCP-9045CDN" \
FALSE "DCP-9055CDN" \
FALSE "DCP-9270CDN" \
FALSE "DCP-J125" \
FALSE "DCP-J132W" \
FALSE "DCP-J140W" \
FALSE "DCP-J152W" \
FALSE "DCP-J172W" \
FALSE "DCP-J315W" \
FALSE "DCP-J4110DW" \
FALSE "DCP-J515W" \
FALSE "DCP-J525W" \
FALSE "DCP-J552DW" \
FALSE "DCP-J715W" \
FALSE "DCP-J725DW" \
FALSE "DCP-J752DW" \
FALSE "DCP-J925DW")
# Display printer models for model FAX
elif [[ ${model} == "FAX" ]]; then
printer=$(zenity --list --radiolist --width=420 --height=420 \
            --title="${LongTitle}" \
            --text="Select your printer model :\n" --column="" --column="" \
FALSE "FAX-1815C" \
FALSE "FAX-1820C" \
FALSE "FAX-1835C" \
FALSE "FAX-1840C" \
FALSE "FAX-1860C" \
FALSE "FAX-1920CN" \
FALSE "FAX-1940CN" \
FALSE "FAX-1960C" \
FALSE "FAX-2440C" \
FALSE "FAX-2480C" \
FALSE "FAX-2580C" \
FALSE "FAX-2820" \
FALSE "FAX-2840" \
FALSE "FAX-2850" \
FALSE "FAX-2890" \
FALSE "FAX-2900" \
FALSE "FAX-2920" \
FALSE "FAX-2940" \
FALSE "FAX-2950" \
FALSE "FAX-2990" \
FALSE "FAX-3800" \
FALSE "FAX-4100" \
FALSE "FAX-4750e" \
FALSE "FAX-5750e")
# Display printer models for model HL
elif [[ ${model} == "HL" ]]; then
printer=$(zenity --list --radiolist --width=400 --height=420 \
            --title="${LongTitle}" \
            --text="Select your printer model :\n" --column="" --column="" \
FALSE "HL-1030" \
FALSE "HL-1118" \
FALSE "HL-1230" \
FALSE "HL-1240" \
FALSE "HL-1250" \
FALSE "HL-1270" \
FALSE "HL-1430" \
FALSE "HL-1440" \
FALSE "HL-1450" \
FALSE "HL-1470N" \
FALSE "HL-1650" \
FALSE "HL-1670N" \
FALSE "HL-1850" \
FALSE "HL-1870N" \
FALSE "HL-2030" \
FALSE "HL-2035" \
FALSE "HL-2040" \
FALSE "HL-2070N" \
FALSE "HL-2130" \
FALSE "HL-2132" \
FALSE "HL-2135W" \
FALSE "HL-2140" \
FALSE "HL-2150N" \
FALSE "HL-2170W" \
FALSE "HL-2220" \
FALSE "HL-2230" \
FALSE "HL-2240" \
FALSE "HL-2240D" \
FALSE "HL-2242D" \
FALSE "HL-2250DN" \
FALSE "HL-2270DW" \
FALSE "HL-2280DW" \
FALSE "HL-2600CN" \
FALSE "HL-2700CN" \
FALSE "HL-3040CN" \
FALSE "HL-3045CN" \
FALSE "HL-3070CW" \
FALSE "HL-3075CW" \
FALSE "HL-3140CW" \
FALSE "HL-3150CDN" \
FALSE "HL-3150CDW" \
FALSE "HL-3170CDW" \
FALSE "HL-3260N" \
FALSE "HL-3450CN" \
FALSE "HL-4040CDN" \
FALSE "HL-4040CN" \
FALSE "HL-4050CDN" \
FALSE "HL-4070CDW" \
FALSE "HL-4140CN" \
FALSE "HL-4150CDN" \
FALSE "HL-4570CDW" \
FALSE "HL-4570CDWT" \
FALSE "HL-5030" \
FALSE "HL-5040" \
FALSE "HL-5050" \
FALSE "HL-5070N" \
FALSE "HL-5130" \
FALSE "HL-5140" \
FALSE "HL-5150D" \
FALSE "HL-5170DN" \
FALSE "HL-5240" \
FALSE "HL-5250DN" \
FALSE "HL-5270DN" \
FALSE "HL-5280DW" \
FALSE "HL-5340D" \
FALSE "HL-5350DN" \
FALSE "HL-5350DNLT" \
FALSE "HL-5370DW" \
FALSE "HL-5370DWT" \
FALSE "HL-5380DN" \
FALSE "HL-5440D" \
FALSE "HL-5450DN" \
FALSE "HL-5470DW" \
FALSE "HL-6050" \
FALSE "HL-6050D" \
FALSE "HL-6050DN" \
FALSE "HL-6180DW" \
FALSE "HL-S7000DN" \
FALSE "HL-7050" \
FALSE "HL-7050N" \
FALSE "HL-8050N")
# Display printer models for model MFC
elif [[ ${model} == "MFC" ]]; then
printer=$(zenity --list --radiolist --width=400 --height=420 \
            --title="${LongTitle}" \
            --text="Select your printer model :\n" \
            --column="" --column="" \
FALSE "MFC-1810" \
FALSE "MFC-1810R" \
FALSE "MFC-1811" \
FALSE "MFC-1813" \
FALSE "MFC-1815" \
FALSE "MFC-1815R" \
FALSE "MFC-1818" \
FALSE "MFC-210C" \
FALSE "MFC-215C" \
FALSE "MFC-230C" \
FALSE "MFC-235C" \
FALSE "MFC-240C" \
FALSE "MFC-250C" \
FALSE "MFC-253CW" \
FALSE "MFC-255CW" \
FALSE "MFC-257CW" \
FALSE "MFC-260C" \
FALSE "MFC-290C" \
FALSE "MFC-295CN" \
FALSE "MFC-297C" \
FALSE "MFC-3220C" \
FALSE "MFC-3240C" \
FALSE "MFC-3320CN" \
FALSE "MFC-3340CN" \
FALSE "MFC-3360C" \
FALSE "MFC-3420C" \
FALSE "MFC-3820CN" \
FALSE "MFC-410CN" \
FALSE "MFC-420CN" \
FALSE "MFC-425CN" \
FALSE "MFC-440CN" \
FALSE "MFC-465CN" \
FALSE "MFC-4800" \
FALSE "MFC-490CW" \
FALSE "MFC-495CW" \
FALSE "MFC-5440CN" \
FALSE "MFC-5460CN" \
FALSE "MFC-5490CN" \
FALSE "MFC-5840CN" \
FALSE "MFC-5860CN" \
FALSE "MFC-5890CN" \
FALSE "MFC-5895CW" \
FALSE "MFC-620CN" \
FALSE "MFC-640CW" \
FALSE "MFC-6490CW" \
FALSE "MFC-660CN" \
FALSE "MFC-665CW" \
FALSE "MFC-6800" \
FALSE "MFC-680CN" \
FALSE "MFC-685CW" \
FALSE "MFC-6890CDW" \
FALSE "MFC-7220" \
FALSE "MFC-7225N" \
FALSE "MFC-7240" \
FALSE "MFC-7290" \
FALSE "MFC-7320" \
FALSE "MFC-7340" \
FALSE "MFC-7345N" \
FALSE "MFC-7360" \
FALSE "MFC-7360N" \
FALSE "MFC-7362N" \
FALSE "MFC-7365DN" \
FALSE "MFC-7420" \
FALSE "MFC-7440N" \
FALSE "MFC-7450" \
FALSE "MFC-7460DN" \
FALSE "MFC-7470D" \
FALSE "MFC-7820N" \
FALSE "MFC-7840N" \
FALSE "MFC-7840W" \
FALSE "MFC-7860DN" \
FALSE "MFC-7860DW" \
FALSE "MFC-790CW" \
FALSE "MFC-795CW" \
FALSE "MFC-820CW" \
FALSE "MFC-8220" \
FALSE "MFC-8370DN" \
FALSE "MFC-8380DN" \
FALSE "MFC-8420" \
FALSE "MFC-8440" \
FALSE "MFC-845CW" \
FALSE "MFC-8460N" \
FALSE "MFC-8480DN" \
FALSE "MFC-8500" \
FALSE "MFC-8510DN" \
FALSE "MFC-8512DN" \
FALSE "MFC-8515DN" \
FALSE "MFC-8520DN" \
FALSE "MFC-8640D" \
FALSE "MFC-8660DN" \
FALSE "MFC-8670DN" \
FALSE "MFC-8680DN" \
FALSE "MFC-8690DW" \
FALSE "MFC-8710DW" \
FALSE "MFC-8712DW" \
FALSE "MFC-8810DW" \
FALSE "MFC-8820D" \
FALSE "MFC-8840D" \
FALSE "MFC-885CW" \
FALSE "MFC-8860DN" \
FALSE "MFC-8870DW" \
FALSE "MFC-8880DN" \
FALSE "MFC-8890DW" \
FALSE "MFC-8910DW" \
FALSE "MFC-8912DW" \
FALSE "MFC-8950DW" \
FALSE "MFC-8952DW" \
FALSE "MFC-8952DWT" \
FALSE "MFC-9010CN" \
FALSE "MFC-9030" \
FALSE "MFC-9070" \
FALSE "MFC-9120CN" \
FALSE "MFC-9125CN" \
FALSE "MFC-9130CW" \
FALSE "MFC-9160" \
FALSE "MFC-9180" \
FALSE "MFC-9320CW" \
FALSE "MFC-9325CW" \
FALSE "MFC-9330CDW" \
FALSE "MFC-9340CDW" \
FALSE "MFC-9420CN" \
FALSE "MFC-9440CN" \
FALSE "MFC-9450CDN" \
FALSE "MFC-9460CDN" \
FALSE "MFC-9465CDN" \
FALSE "MFC-9560CDW" \
FALSE "MFC-9660" \
FALSE "MFC-9700" \
FALSE "MFC-9760" \
FALSE "MFC-9800" \
FALSE "MFC-9840CDW" \
FALSE "MFC-9860" \
FALSE "MFC-9880" \
FALSE "MFC-990CW" \
FALSE "MFC-9970CDW" \
FALSE "MFC-J220" \
FALSE "MFC-J2310" \
FALSE "MFC-J245" \
FALSE "MFC-J2510" \
FALSE "MFC-J265W" \
FALSE "MFC-J270W" \
FALSE "MFC-J280W" \
FALSE "MFC-J285DW" \
FALSE "MFC-J410" \
FALSE "MFC-J410W" \
FALSE "MFC-J415W" \
FALSE "MFC-J425W" \
FALSE "MFC-J430W" \
FALSE "MFC-J4310DW" \
FALSE "MFC-J432W" \
FALSE "MFC-J435W" \
FALSE "MFC-J4410DW" \
FALSE "MFC-J450DW" \
FALSE "MFC-J4510DW" \
FALSE "MFC-J4610DW" \
FALSE "MFC-J470DW" \
FALSE "MFC-J475DW" \
FALSE "MFC-J4710DW" \
FALSE "MFC-J5910DW" \
FALSE "MFC-J615W" \
FALSE "MFC-J625DW" \
FALSE "MFC-J630W" \
FALSE "MFC-J650DW" \
FALSE "MFC-J6510DW" \
FALSE "MFC-J6710DW" \
FALSE "MFC-J6910DW" \
FALSE "MFC-J825DW" \
FALSE "MFC-J835DW" \
FALSE "MFC-J850DW" \
FALSE "MFC-J870DW" \
FALSE "MFC-J875DW")
fi
# Quit immediately if no sélection
if [[ -z ${printer} ]]; then
    exit 1
fi
}

GetConnectionType_GUI() {
# Ask for the type pf connection (USB or network)
connection=$(zenity --list --radiolist --width=400 --height=180 \
                --title="${LongTitle}" \
                --text="Select the type of connection for the printer :\n" \
                --column="" --column="" \
FALSE "USB" \
FALSE "Network")
# Quit immediately if no sélection
if [[ -z ${connection} ]]; then
    exit 1
fi
}

GetIpAddress_GUI() {
# Ask for the printer IP address
ip=$(zenity --entry --width=400 \
            --title="${LongTitle}" \
            --text="Enter the printer IP address :\n")
# execute a ping test
tmp="/tmp/.brother"
for i in 0 25 50 75 100; do
    echo $i
    # if test is ok, create a temp file
    if ping -w1 ${ip} > /dev/null; then
        touch ${tmp}
    fi

done | zenity --progress --width=400 \
            --title="${LongTitle}" \
            --text="Checking connection with device at ${ip}" \
            --percentage=0 --no-cancel --auto-close
# Display a meassage accorfing to the ping test result
if [[ -e ${tmp} ]]; then
    zenity --info --width=400 \
            --title="${LongTitle}" \
            --text="Connection test successful."
    rm ${tmp}
else
    zenity --error --width=400  \
            --title="${LongTitle}" \
            --text="The connection test failed. \
\n\nPlease verify that the IP address is correct. \
\n\nThe install wizard is aborting."
    exit 1
fi
}

ScanKeyToolsInstall_GUI() {
# Ask if we want to install brscan-skey
zenity --question --width=400 \
        --title="${LongTitle}" \
        --text="Do you want to install the Scan-Key tools ?"
if [[ $? == "0" ]]; then
    keytools="4"
else
    keytools="2"
fi
}

ConfirmInstall_GUI() {
# Double check the correctness of the user data entries ..
case ${connection} in
    # Message for USB install
    USB)
    confirm=$(zenity --question --width=400 \
            --title="${LongTitle}" \
            --text="Please confirm that your ${printer} printer \
\nis connected using a USB cable ?")
    ;;
    # Message for Network install
    Network)
    confirm=$(zenity --question --width=400 \
            --title="${LongTitle}" \
            --text="Please confirm that your ${printer} printer \
\nis connected to the network \
\nat the IP address ${ip}?")
    ;;
esac
# Quit the script if answer is no
if [[ $? == 1 ]]; then
    exit 1
fi
}

################################
### INSTALLATION DES PILOTES ###
################################

PreRequisites() {
clear
echo -e ${blue}"Installing pre-requisites"${resetcolor}
# update packets list
echo -e ${green}"Updating the packets list"${resetcolor}
apt-get update 2>&9
# Install the cups packet (if not there yet)
dpkg -l | awk '{print $2}' | grep ^cups$ 1>&9
if [[ $? != 0 ]]; then
    echo -e ${green}"Installing the packet : cups"${resetcolor}
    apt-get install -y cups 2>&9
fi
# Install the sane-util packet (if not there yet)
dpkg -l | awk '{print $2}' | grep ^sane-utils$ 1>&9
if [[ $? != 0 ]]; then
    echo -e ${green}"Installing the paquet : sane-utils"${resetcolor}
    apt-get install -y sane-utils 2>&9
fi
# Install the ia32-libs packet (if not there yet)
# but only for 64bits OS
if [[ ${arch} == "x86_64" || ${arch} == "amd64" ]]; then
    dpkg -l | awk '{print $2}' | grep ^ia32-libs$ 1>&9
    if [[ $? != 0 ]]; then
        echo -e ${green}"Installing the packet : ia32-libs"${resetcolor}
        apt-get install -y ia32-libs 2>&9
    fi
fi
# AppArmor
apparmor=$(which aa-complain)
if [[ -x ${apparmor} ]]; then
    echo -e ${green}"AppArmor profile for CUPS : complain mode"${resetcolor}
    ${apparmor} cupsd 2>&9
fi
# Create a symbolic link  /etc/init.d/lpd ~> /etc/init.d/cups (if not there yet)
# but only for the following printers :
for i in DCP-1000 DCP-1400 DCP-8020 DCP-8025D DCP-8040 DCP-8045D DCP-8060 DCP-8065DN FAX-2850 FAX-2900 FAX-3800 FAX-4100 FAX-4750e FAX-5750e HL-1030 HL-1230 HL-1240 HL-1250 HL-1270N HL-1430 HL-1440 HL-1450 HL-1470N HL-1650 HL-1670N HL-1850 HL-1870N HL-5030 HL-5040 HL-5050 HL-5070N HL-5130 HL-5140 HL-5150D HL-5170DN HL-5240 HL-5250DN HL-5270DN HL-5280DW HL-6050 HL-6050D MFC-4800 MFC-6800 MFC-8420 MFC-8440 MFC-8460N MFC-8500 MFC-8660DN MFC-8820D MFC-8840D MFC-8860DN MFC-8870DW MFC-9030 MFC-9070 MFC-9160 MFC-9180 MFC-9420CN MFC-9660 MFC-9700 MFC-9760 MFC-9800 MFC-9860 MFC-9880; do
    if [[ ${printer} == "$i" ]] && [[ ! -L /etc/init.d/lpd ]]; then
        echo -e ${green}"Creating the symbolic link : \
/etc/init.d/lpd ~> /etc/init.d/cups"${resetcolor}
        ln -s /etc/init.d/cups /etc/init.d/lpd 2>&9
    fi
done
# Install the csh packet (if not there yet)
# but only for the following printers :
for i in DCP-110C DCP-115C DCP-117C DCP-120C DCP-310CN DCP-315CN DCP-340CW FAX-1815C FAX-1820C FAX-1835C FAX-1840C FAX-1920CN FAX-1940CN FAX-2440C MFC-210C MFC-215C MFC-3220C MFC-3240C MFC-3320CN MFC-3340CN MFC-3420C MFC-3820CN MFC-410CN MFC-420CN MFC-425CN MFC-5440CN MFC-5840CN MFC-620CN MFC-640CW MFC-820CW; do
    if [[ ${printer} == "$i" ]] && [[ ! -x /bin/csh ]]; then
        echo -e ${green}"Installing the packet : csh"${resetcolor}
        apt-get install -y csh 2>&9
    fi
done
# Create the folder /var/spool/lpd (if not there yet)
if [[ ! -d /var/spool/lpd ]]; then
    echo -e ${green}"Creating the folder : /var/spool/lpd"${resetcolor}
    mkdir -p /var/spool/lpd 2>&9
    chmod -R +rX /var/spool/lpd
fi
# Create the folder /usr/share/cups/model (if not there yet)
if [[ ! -d /usr/share/cups/model ]]; then
    echo -e ${green}"Creating the folder : /usr/share/cups/model"${resetcolor}
    mkdir -p /usr/share/cups/model  2>&9
    chmod -R +rX /usr/share/cups/model
fi
}


PrinterInstall() {
# look for the printer drivers
if grep -q "${printer}" ${pwd}/url_printer_drivers; then
    echo -e ${blue}"Installing the printer drivers"${resetcolor}
    case ${printer} in
        # Install case MFC-8220
        MFC-8220)
        echo -e ${green}"Downloading file : BR8220_2_GPL.ppd.gz"${resetcolor}
        wget -cP ${pwd}/Packages http://www.brother.com/pub/bsc/linux/dlf/BR8220_2_GPL.ppd.gz
        echo -e ${green}"Installing file : BR8220_2_GPL.ppd"${resetcolor}
        gzip -fd ${pwd}/Packages/BR8220_2_GPL.ppd.gz
        cp ${pwd}/Packages/BR8220_2_GPL.ppd /usr/share/cups/model
        chmod 644 /usr/share/cups/model/BR8220_2_GPL.ppd
        chown root:root /usr/share/cups/model/BR8220_2_GPL.ppd
        ln -sf /usr/share/cups/model/BR8220_2_GPL.ppd /usr/share/ppd/BR8220_2_GPL.ppd
        ;;
        # Install case MFC-8640D
        MFC-8640D)
        echo -e ${green}"Downloading file : BR8640_2_GPL.ppd.gz"${resetcolor}
        wget -cP ${pwd}/Packages http://www.brother.com/pub/bsc/linux/dlf/BR8640_2_GPL.ppd.gz
        echo -e ${green}"Installing file : BR8640_2_GPL.ppd"${resetcolor}
        gzip -fd ${pwd}/Packages/BR8640_2_GPL.ppd.gz
        cp ${pwd}/Packages/BR8640_2_GPL.ppd /usr/share/cups/model
        chmod 644 /usr/share/cups/model/BR8640_2_GPL.ppd
        chown root:root /usr/share/cups/model/BR8640_2_GPL.ppd
        ln -sf /usr/share/cups/model/BR8640_2_GPL.ppd /usr/share/ppd/BR8640_2_GPL.ppd
        ;;
        # Install case HL-2460 and HL 2460N
        HL-2460|HL-2460N)
        echo -e ${green}"Downloading file : BRHL24_2_GPL.ppd.gz"${resetcolor}
        wget -cP ${pwd}/Packages http://www.brother.com/pub/bsc/linux/dlf/BRHL24_2_GPL.ppd.gz
        echo -e ${green}"Installing file : BRHL24_2_GPL.ppd"${resetcolor}
        gzip -fd ${pwd}/Packages/BRHL24_2_GPL.ppd.gz
        cp ${pwd}/Packages/BRHL24_2_GPL.ppd /usr/share/cups/model
        chmod 644 /usr/share/cups/model/BRHL24_2_GPL.ppd
        chown root:root /usr/share/cups/model/BRHL24_2_GPL.ppd
        ln -sf /usr/share/cups/model/BRHL24_2_GPL.ppd /usr/share/ppd/BRHL24_2_GPL.ppd
        ;;
        # Install case HL-2600CN
        HL-2600CN)
        echo -e ${green}"Downloading file : BR2600CN_GPL.ppd.gz"${resetcolor}
        wget -cP ${pwd}/Packages http://www.brother.com/pub/bsc/linux/dlf/BR2600CN_GPL.ppd.gz
        echo -e ${green}"Installing file : BR2600CN_GPL.ppd"${resetcolor}
        gzip -fd ${pwd}/Packages/BR2600CN_GPL.ppd.gz
        cp ${pwd}/Packages/BR2600CN_GPL.ppd /usr/share/cups/model
        chmod 644 /usr/share/cups/model/BR2600CN_GPL.ppd
        chown root:root /usr/share/cups/model/BR2600CN_GPL.ppd
        ln -sf /usr/share/cups/model/BR2600CN_GPL.ppd /usr/share/ppd/BR2600CN_GPL.ppd
        ;;
        # Install case HL-2700CN
        HL-2700CN)
        echo -e ${green}"Downloading file : BR2700CN_GPL.ppd.gz"${resetcolor}
        wget -cP ${pwd}/Packages http://www.brother.com/pub/bsc/linux/dlf/BR2700_2_GPL.ppd.gz
        echo -e ${green}"Installing file : BR2700CN_GPL.ppd"${resetcolor}
        gzip -fd ${pwd}/Packages/BR2700_2_GPL.ppd.gz
        cp ${pwd}/Packages/BR2700_2_GPL.ppd /usr/share/cups/model
        chmod 644 /usr/share/cups/model/BR2700_2_GPL.ppd
        chown root:root /usr/share/cups/model/BR2700_2_GPL.ppd
        ln -sf /usr/share/cups/model/BR2700_2_GPL.ppd /usr/share/ppd/BR2700_2_GPL.ppd
        ;;
        # Install case HL-3260N
        HL-3260N)
        echo -e ${green}"downloading file : BRHL32_3_GPL.ppd.gz"${resetcolor}
        wget -cP ${pwd}/Packages http://www.brother.com/pub/bsc/linux/dlf/BRHL32_3_GPL.ppd.gz
        echo -e ${green}"Installing file : BRHL32_3_GPL.ppd"${resetcolor}
        gzip -fd ${pwd}/Packages/BRHL32_3_GPL.ppd.gz
        cp ${pwd}/Packages/BRHL32_3_GPL.ppd /usr/share/cups/model
        chmod 644 /usr/share/cups/model/BRHL32_3_GPL.ppd
        chown root:root /usr/share/cups/model/BRHL32_3_GPL.ppd
        ln -sf /usr/share/cups/model/BRHL32_3_GPL.ppd /usr/share/ppd/BRHL32_3_GPL.ppd
        ;;
        # Install case HL-3450CN
        HL-3450CN)
        echo -e ${green}"Downloading file : BR3450CN_GPL.ppd.gz"${resetcolor}
        wget -cP ${pwd}/Packages http://www.brother.com/pub/bsc/linux/dlf/BR3450CN_GPL.ppd.gz
        echo -e ${green}"Installing file : BR3450CN_GPL.ppd"${resetcolor}
        gzip -fd ${pwd}/Packages/BR3450CN_GPL.ppd.gz
        cp ${pwd}/Packages/BR3450CN_GPL.ppd /usr/share/cups/model
        chmod 644 /usr/share/cups/model/BR3450CN_GPL.ppd
        chown root:root /usr/share/cups/model/BR3450CN_GPL.ppd
        ln -sf /usr/share/cups/model/BR3450CN_GPL.ppd /usr/share/ppd/BR3450CN_GPL.ppd
        ;;
        # Install case HL-7050 and HL-7050N
        HL-7050|HL-7050N)
        echo -e ${green}"downloading file : BR7050_2_GPL.ppd.gz"${resetcolor}
        wget -cP ${pwd}/Packages http://www.brother.com/pub/bsc/linux/dlf/BR7050_2_GPL.ppd.gz
        echo -e ${green}"Installing file : BR7050_2_GPL.ppd"${resetcolor}
        gzip -fd ${pwd}/Packages/BR7050_2_GPL.ppd.gz
        cp ${pwd}/Packages/BR7050_2_GPL.ppd /usr/share/cups/model
        chmod 644 /usr/share/cups/model/BR7050_2_GPL.ppd
        chown root:root /usr/share/cups/model/BR7050_2_GPL.ppd
        ln -sf /usr/share/cups/model/BR7050_2_GPL.ppd /usr/share/ppd/BR7050_2_GPL.ppd
        ;;
        # Install case HL-S7000DN
        HL-S7000DN)
        echo -e ${green}"Downloading file : BRP7000E_GPL.PPD.gz"${resetcolor}
        wget -cP ${pwd}/Packages http://www.brother.com/pub/bsc/linux/dlf/BRP7000E_GPL.PPD.gz
        echo -e {green}"Installing file : BRP7000E_GPL.PPD"${resetcolor}
        gzip -fd ${pwd}/Packages/BRP7000E_GPL.PPD.gz
        cp ${pwd}/Packages/BRP7000E_GPL.PPD /usr/share/cups/model/BRP7000E_GPL.ppd
        chmod 644 /usr/share/cups/model/BRP7000E_GPL.ppd
        chown root:root /usr/share/cups/model/BRP7000E_GPL.ppd
        ln -sf /usr/share/cups/model/BRP700E_GPL.ppd /usr/share/ppd/BRP7000E_GPL.ppd
        ;;
        # Install case HL-8050N
        HL-8050N)
        echo -e ${green}"Downloading file : BR8050_2_GPL.ppd.gz"${resetcolor}
        wget -cP ${pwd}/Packages http://www.brother.com/pub/bsc/linux/dlf/BR8050_2_GPL.ppd.gz
        echo -e ${green}"Installing file : BR8050_2_GPL.ppd.gz"${resetcolor}
        gzip -fd ${pwd}/Packages/BR8050_2_GPL.ppd.gz
        cp ${pwd}/Packages/BR8050_2_GPL.ppd /usr/share/cups/model
        chmod 644 /usr/share/cups/model/BR8050_2_GPL.ppd
        chown root:root /usr/share/cups/model/BR8050_2_GPL.ppd
        ln -sf /usr/share/cups/model/BR8050_2_GPL.ppd /usr/share/ppd/BR8050_2_GPL.ppd
        ;;
        # Install case of other models
        *)
        n=0
        for url in $(grep -A 2 ${printer} ${pwd}/url_printer_drivers|grep -v ${printer}); do
            package[${n}]=$(basename ${url})
            echo -e ${green}"Downloading file : $(basename ${url})"${resetcolor}
            wget -cP ${pwd}/Packages ${url}
            echo -e ${green}"Installing file : $(basename ${url})"${resetcolor}
            dpkg -i --force-all "${pwd}/Packages/$(basename ${url})" 2>&9
            n=${n}+1
        done
        ;;
    esac
    # Configurer l'imprimante
    PrinterConfig
fi
}

ScannerInstall() {
# Looking for the scanner drivers
if grep -q "${printer}" ${pwd}/url_scanner_drivers; then
    echo -e ${blue}"Installing scanner drivers"${resetcolor}
    case ${arch} in
        # Install for 32 bits machine
        i386|i486|i686)
            for url in $(grep -A ${keytools} ${printer} ${pwd}/url_scanner_drivers|grep "i386"); do
                echo -e ${green}"Downloading file : $(basename ${url})"${resetcolor}
                wget -cP ${pwd}/Packages ${url}
                echo -e ${green}"Installation of the file : $(basename ${url})"${resetcolor}
                dpkg -i --force-all "${pwd}/Packages/$(basename ${url})" 2>&9
            done
        ;;
        # Install for 64 bits machine
        x86_64|amd64)
            for url in $(grep -A ${keytools} ${printer} ${pwd}/url_scanner_drivers|grep "amd64"); do
                echo -e ${green}"Downloading file : $(basename ${url})"${resetcolor}
                wget -cP ${pwd}/Packages ${url}
                echo -e ${green}"Installation of the file : $(basename ${url})"${resetcolor}
                dpkg -i --force-all "${pwd}/Packages/$(basename ${url})" 2>&9
            done
        ;;
    esac
    # Download and install brother-udev-rule-type1-1.0.0-1.all.deb packet
    url="http://www.brother.com/pub/bsc/linux/dlf/brother-udev-rule-type1-1.0.0-1.all.deb"
    echo -e ${green}"Downloading file : $(basename ${url})"${resetcolor}
    wget -cP ${pwd}/Packages ${url}
    echo -e ${green}"Installation of the file : $(basename ${url})"${resetcolor}
    dpkg -i "${pwd}/Packages/$(basename ${url})" 2>&9
    # Configure the scanner
    ScannerConfig
fi
}

FaxInstall() {
# Look for the Fax drivers
if grep -q "${printer}" ${pwd}/url_fax_drivers; then
    echo -e ${blue}"Installing fax drivers"${resetcolor}
    # Installing drivers
    for url in $(grep -A 2 ${printer} ${pwd}/url_fax_drivers|grep -v ${printer}); do
        echo -e ${green}"Downloading the file : $(basename ${url})"${resetcolor}
        wget -cP ${pwd}/Packages ${url}
        echo -e ${green}"Installing the file : $(basename ${url})"${resetcolor}
        dpkg -i --force-all "${pwd}/Packages/$(basename ${url})" 2>&9
    done
    # Configure the Fax
    FaxConfig
fi
}

PrinterConfig() {
# Get the name of the printer
printername=$(echo ${printer}|sed 's/'-'//g'|tr [a-z] [A-Z])
# Chercher le fichier ppd à utiliser
case ${printer} in
    # MFC-8220
    MFC-8220)
    ppd="/usr/share/cups/model/BR8220_2_GPL.ppd"
    ;;
    # HL-2406 et HL-2460N
    HL-2460|HL-2460N)
    ppd="/usr/share/cups/model/BRHL24_2_GPL.ppd"
    ;;
    # HL-2600CN
    HL-2600CN)
    ppd="/usr/share/cups/model/BR2600CN_GPL.ppd"
    ;;
    # HL-2700CN
    HL-2700CN)
    ppd="/usr/share/cups/model/BR2700_2_GPL.ppd"
    ;;
    # HL-3260N
    HL-3260N)
    ppd="/usr/share/cups/model/BRHL32_3_GPL.ppd"
    ;;
    # HL-3450CN
    HL-3450CN)
    ppd="/usr/share/cups/model/BR3450CN_GPL.ppd"
    ;;
    # HL-S7000DN
    HL-S7000DN)
    ppd="/usr/share/cups/model/BRP7000E_GPL.ppd"
    ;;
    # HL-7050 et HL-7050N
    HL-7050|HL-7050N)
    ppd="/usr/share/cups/model/BR7050_2_GPL.ppd"
    ;;
    # HL-8050N
    HL-8050N)
    ppd="/usr/share/cups/model/BR8050_2_GPL.ppd"
    ;;
    # DCP-115C, DCP-117C, DCP-120C, DCP-315CN, DCP-340CW, MFC-210C, MFC-215C, MFC-425CN, MFC-640CW and MFC-820CW
    DCP-115C|DCP-117C|DCP-120C|DCP-315CN|DCP-340CW|MFC-210C|MFC-215C|MFC-425CN|MFC-640CW|MFC-820CW)
    ppd="/usr/share/cups/model/brmfc210c_cups.ppd"
    ;;
    # All other models
    *)
    ppd=$(find /usr/share/cups/model -iname *${printername}*) 2>&9
    if [[ -z ${ppd} ]]; then
        ppd=$(find /opt/brother/Printers -iname *${printername}*) 2>&9
    fi
    ;;
esac
# Fix the problem raising when some printers using the same drivers as MFC-210C
# by deleting the printer named MFC210C
case ${printer} in
    DCP-115C|DCP-117C|DCP-120C|DCP-315CN|DCP-340CW|MFC-215C|MFC-425CN|MFC-640CW|MFC-820CW)
    lpadmin -x MFC210C 2>&9
    ;;
# Fix the problem raising when some printers using the same drivers as MFC-1818
# by deleting the printer named MFC1818
    MFC-1810|MFC-1810R|MFC-1811|MFC-1815|MFC-1815R)
    lpadmin -x MFC-1818 2>&9
    ;;
esac
# Ajouter l'imprimante
case ${connection} in
    # USB connected printer
    USB)
    lpadmin -p ${printername} -E -v usb://Brother/${printer} -P ${ppd} 2>&9
    ;;
    # Network connected printer
    Network)
    lpadmin -p ${printername} -E -v lpd://${ip}/binary_p1 -P ${ppd} 2>&9
    ;;
esac
# Find out the executable to configure the printer options
brprintconf=$(for num in 0 1; do dpkg --contents ${pwd}/Packages/${package[${num}]} | awk '{print $6}' | grep brprintconf | sed 's/^.\//\//'; done)
case ${brprintconf} in
        /usr/bin/brprintconf)
            ${brprintconf} -P ${printername} -pt A4 2>&9
        ;;
        /usr/bin/brprintconfij)
            ${brprintconf} -P ${printername} -pt A4 2>&9
        ;;
        /usr/bin/brprintconfij2)
            ${brprintconf} -P ${printername} -pt A4 2>&9
        ;;
        /usr/bin/brprintconfcl1)
                ${brprintconf} -P ${printername} -pt A4 2>&9
        ;;
        /usr/bin/brprintconflsr2)
                ${brprintconf} -P ${printername} -pt A4 2>&9
        ;;
        *)
                ${brprintconf} -pt A4 2>&9 || ${brprintconf} -Paper A4 2>&9
        ;;
esac
}

ScannerConfig() {
# Case of USB scanner
if [[ ${connection} == "USB" ]]; then
    # Adding udev rule (if not there yet)
    if [[ ! $(grep -q "Brother" ${rules}) ]]; then
        echo -e '\n# Brother\nATTRS{idVendor}=="04f9", ENV{libsane_matched}="yes"'|tee -a ${rules}  1>&9
    fi
# Case of network scanner
elif [[ ${connection} == "Network" ]]; then
    # Add an entry for the Network scanner
    # brsaneconfig
    if [[ -x /usr/bin/brsaneconfig ]]; then
        brsaneconfig -a name="SCANNER" model="${printer}" ip=${ip} 2>&9
    # brsaneconfig2
    elif [[ -x /usr/bin/brsaneconfig2 ]]; then
        brsaneconfig2 -a name="SCANNER" model="${printer}" ip=${ip} 2>&9
    # brsaneconfig3
    elif [[ -x /usr/bin/brsaneconfig3 ]]; then
        brsaneconfig3 -a name="SCANNER" model="${printer}" ip=${ip} 2>&9
    # brsaneconfig4
    elif [[ -x /usr/bin/brsaneconfig4 ]]; then
        sed -i '/Support Model/a\
0x029a, 117, 1, "MFC-8690DW", 133, 4\
0x0279, 14, 2, "DCP-J525W"\
0x027b, 13, 2, "DCP-J725DW"\
0x027d, 13, 2, "DCP-J925DW"\
0x027f, 14, 1, "MFC-J280W"\
0x028f, 13, 1, "MFC-J425W"\
0x0281, 13, 1, "MFC-J430W"\
0x0280, 13, 1, "MFC-J435W"\
0x0282, 13, 1, "MFC-J625DW"\
0x0283, 13, 1, "MFC-J825DW"\
0x028d, 13, 1, "MFC-J835DW"' /usr/local/Brother/sane/Brsane4.ini 2>&9
        brsaneconfig4 -a name="SCANNER" model="${printer}" ip=${ip} 2>&9
    fi
fi
FixScannerConfig
}

FaxConfig() {
# Change the access rights of the file brfaxfilter
if [[ -e /usr/lib/cups/filter/brfaxfilter ]]; then
    chmod 755 /usr/lib/cups/filter/brfaxfilter 2>&9
    service cups restart 2>&9
fi
# Define the fax URI
case ${connection} in
    # USB Fax
    USB)
        lpadmin -p BRFAX -v usb://Brother/${printer} 2>&9
    ;;
    # Network Fax
    Network)
        lpadmin -p BRFAX -v lpd://${ip}/binary_p1 2>&9
    ;;
esac
}

FixScannerConfig() {
# Fix scanner problem with Ubuntu 64 bits >= 11.10
if [[ "${codename}" == "oneiric" || "${codename}" == "precise" || "${codename}" == "quantal"  || "${codename}" == "raring" ]] && [[ ${arch} == "x86_64" ]]; then
    # brscan
    if [[ -e /usr/bin/brsaneconfig ]]; then
        cp /usr/lib64/libbrcolm.so.1.0.1 /usr/lib/
        cp /usr/lib64/libbrscandec.so.1.0.0 /usr/lib/
        cp /usr/lib64/sane/libsane-brother.so.1.0.7 /usr/lib/sane/
        cp /usr/lib64/sane/libsane-brother.so /usr/lib/sane/
        cp /usr/lib64/sane/libsane-brother.so.1 /usr/lib/sane/
        cp /usr/lib64/libbrscandec.so.1 /usr/lib/
        cp /usr/lib64/libbrcolm.so /usr/lib/
        cp /usr/lib64/libbrcolm.so.1 /usr/lib/
        cp /usr/lib64/libbrscandec.so /usr/lib/
    # brscan2
    elif [[ -e /usr/bin/brsaneconfig2 ]]; then
        cp /usr/lib64/libbrscandec2.so.1.0.0  /usr/lib/
        cp /usr/lib64/sane/libsane-brother2.so.1.0.7 /usr/lib/sane/
        cp /usr/lib64/sane/libsane-brother2.so.1 /usr/lib/sane/
        cp /usr/lib64/sane/libsane-brother2.so /usr/lib/sane/
        cp /usr/lib64/libbrcolm2.so.1.0.1 /usr/lib/
        cp /usr/lib64/libbrcolm2.so /usr/lib/
        cp /usr/lib64/libbrscandec2.so.1 /usr/lib/
        cp /usr/lib64/libbrscandec2.so /usr/lib/
        cp /usr/lib64/libbrcolm2.so.1 /usr/lib/

    # Brother did update the brscan3 and brscan4 packets
    # We don't need anymore to copy these libraries.
    #
    # brscan3
    #   elif [[ -e /usr/bin/brsaneconfig3 ]]; then
    #       cp /usr/lib64/libbrscandec3.so.1.0.0 /usr/lib/
    #       cp /usr/lib64/sane/libsane-brother3.so.1.0.7 /usr/lib/sane/
    #       cp /usr/lib64/sane/libsane-brother3.so.1 /usr/lib/sane/
    #       cp /usr/lib64/sane/libsane-brother3.so /usr/lib/sane/
    #       cp /usr/lib64/libbrscandec3.so /usr/lib/
    #       cp /usr/lib64/libbrscandec3.so.1 /usr/lib/
    # brscan4
    #elif [[ -x /usr/bin/brsaneconfig4 ]]; then
    #   cp /usr/lib64/sane/libsane-brother4.so.1.0.7 /usr/lib/sane/
    #   cp /usr/lib64/sane/libsane-brother4.so /usr/lib/sane/
    #   cp /usr/lib64/sane/libsane-brother4.so.1 /usr/lib/sane/
    fi
fi
}

# Installation in Terminal mode (CLI)
#====================================
Cli_mode() {

echo "CLI_Mode entry"
set -x
Help
CheckSourceList
GetModel
GetPrinter
GetConnectionType
# Ask also for the IP if this is a network connection
if [[ ${connection} == "Network" ]]; then
    GetIpAddress
fi
# Handle the ScanKey Tools
if grep -q "${printer}" ${pwd}/url_scanner_drivers; then
    ScanKeyToolsInstall
fi
ConfirmInstall
}

# Installation in Graphical mode (GUI)
#=====================================
Gui_mode() {
CheckSourceList_GUI
GetModel_GUI
GetPrinter_GUI
GetConnectionType_GUI
# Ask also for the IP if this is a network connection
if [[ ${connection} == "Network" ]]; then
    GetIpAddress_GUI
fi
# Handle the ScanKey Tools
if grep -q "${printer}" ${pwd}/url_scanner_drivers; then
    ScanKeyToolsInstall_GUI
fi
ConfirmInstall_GUI
}

####################
### Script entry ###
####################


GetUID
Log
CheckZenity
case ${InstallWizard} in
    # Using the wizard in "Terminal" mode
    whiptail)
        Cli_mode
    ;;
    # Using the wizard in "GUI" mode
    zenity)
        Gui_mode
    ;;
esac
# all data available. Run the install now.
PreRequisites
PrinterInstall
ScannerInstall
FaxInstall
# Change the file ownership within the brother folder

chown -R ${who}:${who} ${pwd}
# Purge the "sudo" cache
sudo -k

echo "${LongTitle} completed."
exit 0
