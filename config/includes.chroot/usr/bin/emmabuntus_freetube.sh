#! /bin/bash

# emmabuntus_freetube.sh --
#
#   This file permits to install Freetube for the Emmabuntüs distrib.
#
#   Created on 2010-22 by collectif Emmabuntüs (contact@emmabuntus.org).
#
#   It was validate on Debian 12 XFCE/LXQt.
#
#   Home web site : https://emmabuntus.org/
#
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


################################################################################

. "$HOME/.config/user-dirs.dirs"

clear

nom_distribution="Emmabuntus Debian Edition 5"

nom_logiciel_affichage="FreeTube"

nom_paquet=freetube

URL="http://freetubex64.emmabuntus.org/"

dir_home=${HOME}

base_distribution="bookworm"

delai_fenetre=20


if [[ $(uname -m | grep -e "x86_64") ]] ; then

    if [[ $(dpkg --get-selections ${nom_paquet} 2> /dev/null | grep -e "install") ]] ; then

        ${nom_paquet} &

    else

        #  chargement des messages
        . /opt/Install_non_free_softwares/emmabuntus_messages.sh


        if test -z "$message_charge" ; then

            echo "# $nom_logiciel_affichage software installation canceled : Load message KO"

            exit 0

        fi



        export WINDOW_DIALOG_INSTALL_NO_FREE_SOFTWARE='<window title="'${nom_logiciel_affichage}'" icon-name="gtk-dialog-question" resizable="false">
        <vbox spacing="0">

        <text use-markup="true" wrap="false" xalign="0" justify="3">
        <input>echo "'$message_demarrage_soft'" | sed "s%\\\%%g"</input>
        </text>

        <hbox spacing="10" space-expand="false" space-fill="false">
        <button cancel></button>
        <button can-default="true" has-default="true" use-stock="true" is-focus="true">
        <label>gtk-ok</label>
        <action>exit:OK</action>
        </button>
        </hbox>

        </vbox>
        </window>'

        MENU_DIALOG_DIALOG_INSTALL_NO_FREE_SOFTWARE="$(gtkdialog --center --program=WINDOW_DIALOG_INSTALL_NO_FREE_SOFTWARE)"

        eval ${MENU_DIALOG_DIALOG_INSTALL_NO_FREE_SOFTWARE}

        if [[ ${EXIT} != "OK" ]]
        then

            echo "# $nom_logiciel_affichage software installation canceled"

            exit 1

        else

            if [[ $(ping -w 1 sourceforge.net | grep "1 received") ]] ; then

                echo "Internet is a live"

            else

                echo "Internet wasn't a live !!"

                export WINDOW_DIALOG_NO_INTERNET_INSTALL_KO='<window title="'${nom_logiciel_affichage}'" icon-name="gtk-dialog-error" resizable="false">
                <vbox spacing="0">

                <text use-markup="true" wrap="false" xalign="0" justify="3">
                <input>echo "'$message_non_internet_install_ko'" | sed "s%\\\%%g"</input>
                </text>

                <hbox spacing="10" space-expand="false" space-fill="false">
                <button can-default="true" has-default="true" use-stock="true" is-focus="true">
                <label>gtk-ok</label>
                <action>exit:OK</action>
                </button>
                </hbox>

                </vbox>
                </window>'

                MENU_DIALOG_DIALOG_NO_INTERNET_INSTALL_KO="$(gtkdialog --center --program=WINDOW_DIALOG_NO_INTERNET_INSTALL_KO)"

                exit 1

            fi

            (

            #Désactivation de Gmenu de Cairo-dock pendant l'installation de logiciel pour éviter les fenêtres de notifications
            /usr/bin/cairo_dock_gmenu_off.sh

            wget -c ${URL} -O ~/freetube_amd64.deb

            echo "# ${install_title}"

            pkexec /usr/bin/emmabuntus_freetube_exec.sh ${dir_home}

            if [[ $(dpkg --get-selections ${nom_paquet} 2> /dev/null | grep -e "install") ]] ; then

                if [ -f ~/freetube_amd64.deb ] ; then

                    echo "Remove package"
                    rm ~/freetube_amd64.deb

                fi

                ${nom_paquet} &

            else

                zenity --error --timeout=$delai_fenetre \
                --text="<span color=\"red\">$install_ko</span>"

                exit 2

            fi

            #Activation de Gmenu de Cairo-dock
            /usr/bin/cairo_dock_gmenu_on.sh

            echo "100" ;

            ) |
            zenity --progress --pulsate \
            --title="$install_title" \
            --text="$install_text" \
            --width=300 \
            --percentage=0 --auto-close --no-cancel
                if [ $? = "1" ]
                then

                #Activation de Gmenu de Cairo-dock
                /usr/bin/cairo_dock_gmenu_on.sh

                zenity --error --timeout=$delai_fenetre \
                    --text="$install_cancel"
                else
                echo "# Install Completed"
                fi

        fi

    fi

fi
