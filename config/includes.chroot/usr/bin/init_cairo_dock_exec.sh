#! /bin/bash

# init_cairo_dock_exec.sh --
#
#   This file permits to init cairo-dock for Emmabuntüs distribution
#
#   Created on 2010-22 by collectif Emmabuntüs (contact@emmabuntus.org).
#
#   It was checked and validated on Debian 12 XFCE/LXQt.
#
#   Home web site : https://emmabuntus.org/
#
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


################################################################################

if [[ ${1} == root ]] ; then
dir_user=/root
else
dir_user=/home/${1}
fi

sudo cp -R /etc/skel/.config/cairo-dock-language/ ${dir_user}/.config/
sudo chown -R ${1}:${1} ${dir_user}/.config/cairo-dock-language/


