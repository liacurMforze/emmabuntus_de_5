#! /bin/bash

# start_cairo_dock.sh --
#
#   This file permits to start cairo-dock and modified the mode of display the dock.
#
#   Created on 2010-22 by collectif Emmabuntüs (contact@emmabuntus.org).
#
#   It was validate on Debian 12 XFCE/LXQt.
#
#   Home web site : https://emmabuntus.org/
#
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


################################################################################


clear

###Pour exporter la librairie de gettext.
set -a
source gettext.sh
set +a
export TEXTDOMAIN=emmabuntus
export TEXTDOMAINDIR="/usr/share/locale"
. gettext.sh
emmabuntus=$0


nom_distribution="Emmabuntus Debian Edition 5"
repertoire_emmabuntus=~/.config/emmabuntus
env_emmabuntus=${repertoire_emmabuntus}/env_emmabuntus
fichier_affichage_config=${repertoire_emmabuntus}/display_emmabuntus_cairodock_defaut.txt
repertoire_cairodock=~/.config/cairo-dock-language
cairodock_config=cairo-dock.conf
cairodock_config_simple=cairo-dock-simple.conf
cairodock_choice_dock=cairo-dock-choise-dock.conf
cairodock_choice_dock_old=cairo-dock-choise-dock-old.conf



langue=`echo $LANG | cut -d_ -f 1 `



CST_RATIO_5_4=125    ; # 1280x1024
CST_RATIO_4_3=133    ; # valeur réelle = 1.333 ; # 1280x960
CST_RATIO_16_10=160  ; # 1280x800
CST_RATIO_16_9=177   ; # valeur réelle = 1.777 ; # 1280x720


#  chargement des variables d'environnement
. ${env_emmabuntus}

# Définition par défaut des messages en Anglais


   msg_dock_all="$(eval_gettext 'All          :')"
msg_dock_simple="$(eval_gettext 'Simple   :')"
  msg_dock_kids="$(eval_gettext 'Basic     :')"

comment_dock_all="$(eval_gettext 'For those wanting access to all applications')"
comment_dock_simple="$(eval_gettext 'For newbies and my dear old aunt ;-)')"
comment_dock_kids="$(eval_gettext 'For children and very beginners')"

message_info_dock_cairo_defaut="\n\
$(eval_gettext 'You are using a version of Cairo-Dock which is not the Emmabuntüs default one.')     \n\
\n\
$(eval_gettext 'Do you want to replace it by the Emmabuntüs default version?')\n\
\n\
\<span color=\'"'red\'"'>$(eval_gettext 'Warning:')\</span> $(eval_gettext 'This will delete your version of the Cairo-Dock.')"

message_affichage_info_dock_cairo_defaut="\n\
$(eval_gettext 'Do you want to mask this information window at next startup?')\n\
$(eval_gettext 'which is not the one used by default for the next boot emmabuntüs?     ')"



 if ( [[ $LANG == en* ]] || [[ $LANG == fr* ]] || [[ $LANG == es* ]] || [[ $LANG == de* ]] || [[ $LANG == pt* ]] || [[ $LANG == it* ]] || [[ $LANG == da* ]] || [[ $LANG == ar* ]] )
 then
     echo "Language supported in Dock"
 else
     echo "Language unsupported in Dock"
     langue=en
 fi



if  ps -A | grep xcompmgr
then
    echo "Xcompmgr already started"
else
    picom &
fi



# Détermination de la résolution de l'écran

x=`xwininfo -root | grep Width: |cut -d: -f 2`
y=`xwininfo -root | grep Height: |cut -d: -f 2`

ratio_ecran=$(($(( $x * 100 ))/$y))


if test $ratio_ecran -le $(( $(( $CST_RATIO_4_3 + $CST_RATIO_5_4 )) / 2 ))
then
   ecran=ecran_5_4

elif test $ratio_ecran -le $(( $(( $CST_RATIO_16_10 + $CST_RATIO_4_3 )) / 2 ))
then
   ecran=ecran_4_3

elif test $ratio_ecran -le $(( $(( $CST_RATIO_16_9 + $CST_RATIO_16_10 )) / 2 ))
then
   ecran=ecran_16_10

elif test $ratio_ecran -ge $(( $(( $CST_RATIO_16_9 + $CST_RATIO_16_10 )) / 2 ))
then
   ecran=ecran_16_9

else
   echo "Ecran résolution inconnue !!!"
   ecran=ecran_4_3

fi


echo "Format de l'ecran = $ecran"

echo "Langue = $langue"

# Vérification d'un répertoire Cairo-dock par défaut n'est pas utilisé à la place de la version d'Emmabuntüs
if ! test -f ~/.config/cairo-dock/current_theme/emmabuntus_info.txt
then
    # vérification que le répertoire Cairo-dock par défaut existe
    if test -d ~/.config/cairo-dock
    then


        if ! test -f $fichier_affichage_config
        then


            export WINDOW_DIALOG_DOCK_CAIRO_DEFAUT='<window title="'$(eval_gettext 'Choose which dock version')'" icon-name="gtk-dialog-question" resizable="false">
            <vbox spacing="0">

            <text use-markup="true" wrap="false" xalign="0" justify="3">
            <input>echo "'$message_info_dock_cairo_defaut'" | sed "s%\\\%%g"</input>
            </text>

            <hbox spacing="10" space-expand="false" space-fill="false">
            <button cancel></button>
            <button ok></button>
            </hbox>

            </vbox>
            </window>'

            MENU_DIALOG_DOCK_CAIRO_DEFAUT="$(gtkdialog --center --program=WINDOW_DIALOG_DOCK_CAIRO_DEFAUT)"

            eval ${MENU_DIALOG_DOCK_CAIRO_DEFAUT}


            if [ ${EXIT} == "OK" ]
            then
                rm -r ~/.config/cairo-dock

            else

            export WINDOW_DIALOG_REMOVE_INFO_DOCK_CAIRO_DEFAUT='<window title="'${titre}'" icon-name="gtk-dialog-question" resizable="false">
            <vbox spacing="0">

            <text use-markup="true" wrap="false" xalign="0" justify="3">
            <input>echo "'$message_affichage_info_dock_cairo_defaut'" | sed "s%\\\%%g"</input>
            </text>

            <hbox spacing="10" space-expand="false" space-fill="false">
            <button cancel></button>
            <button ok></button>
            </hbox>

            </vbox>
            </window>'

            MENU_DIALOG_REMOVE_INFO_DOCK_CAIRO_DEFAUT="$(gtkdialog --center --program=WINDOW_DIALOG_REMOVE_INFO_DOCK_CAIRO_DEFAUT)"

            eval ${MENU_DIALOG_REMOVE_INFO_DOCK_CAIRO_DEFAUT}


                if [ ${EXIT} == "OK" ]
                then
                    echo $nom_distribution > $fichier_affichage_config
                    echo "Fichier servant à ne pas afficher la fenêtre d'information de l'utilisation d'une version de Cairo-dock non utilisée par défaut dans Emmabuntüs" >> $fichier_affichage_config
                fi
            fi
        fi
    fi
fi

# test de la langue afin de créer le lien symbolique vers le répertoire cairo-dock dans la bonne langue
    if $(test -d ${repertoire_cairodock}/cairo-dock-${langue}-simple) || $(test -d ${repertoire_cairodock}/cairo-dock-${langue}-kids)
    then
        echo "1"

        display_dialog_cairo_dock=true

        if ! test -f ${repertoire_cairodock}/${cairodock_choice_dock}
        then


            if $(test -d ${repertoire_cairodock}/cairo-dock-${langue}-simple) && $(test -d ${repertoire_cairodock}/cairo-dock-${langue}-kids)
             then

                select_dock_all=false ; select_dock_simple=true ; select_dock_kids=false ; visible_dock_simple=true ; visible_dock_kids=true

                if test -f ${repertoire_cairodock}/${cairodock_choice_dock_old}
                then

                    choix=`cat ${repertoire_cairodock}/${cairodock_choice_dock_old} | grep "Dock=" | cut -d"=" -f 2`

                    echo "$choix"

                    if [[ $choix == *All* ]]
                    then

                        select_dock_all=true ; select_dock_simple=false ; select_dock_kids=false

                    elif [[ $choix == *Kids* ]]
                    then

                        select_dock_all=false ; select_dock_simple=false ; select_dock_kids=true

                    fi

                fi



            elif  $(test -d ${repertoire_cairodock}/cairo-dock-${langue}-simple)
             then

                select_dock_all=false ; select_dock_simple=true ; visible_dock_simple=true ; visible_dock_kids=false

                if test -f ${repertoire_cairodock}/${cairodock_choice_dock_old}
                then

                    choix=`cat ${repertoire_cairodock}/${cairodock_choice_dock_old} | grep "Dock=" | cut -d"=" -f 2`

                    echo "$choix"

                    if [[ $choix == *All* ]]
                    then
                        select_dock_all=true ; select_dock_simple=false

                    fi

                fi


            elif $(test -d ${repertoire_cairodock}/cairo-dock-${langue}-kids)
             then

                select_dock_all=false ; select_dock_kids=true ;  visible_dock_simple=false ; visible_dock_kids=true

                if test -f ${repertoire_cairodock}/${cairodock_choice_dock_old}
                then

                    choix=`cat ${repertoire_cairodock}/${cairodock_choice_dock_old} | grep "Dock=" | cut -d"=" -f 2`

                    echo "$choix"

                    if [[ $choix == *All* ]]
                    then
                        select_dock_all=true ; select_dock_kids=false

                    fi

                fi

            else

                choix="All"
                display_dialog_cairo_dock=false

            fi

            if [ ${display_dialog_cairo_dock} == "true" ]
            then

                export WINDOW_DIALOG_CAIRO_DOCK='<window title="'${titre}'" icon-name="gtk-dialog-question" resizable="false">
                <vbox spacing="5" space-fill="true" space-expand="true">

                   <text use-markup="true" wrap="false">
                   <input>echo "\n '$(eval_gettext 'Please select the dock version which best fits your needs:')'  " | sed "s%\\\%%g"</input>
                   </text>

                  <frame>
                  <radiobutton>
                    <variable>select_dock_all</variable>
                    <label>'${msg_dock_all}' '${comment_dock_all}'     </label>
                    <default>'${select_dock_all}'</default>
                   </radiobutton>
                   <radiobutton visible="'${visible_dock_simple}'">
                    <variable>select_dock_simple</variable>
                    <label>'${msg_dock_simple}' '${comment_dock_simple}'     </label>
                    <default>'${select_dock_simple}'</default>
                   </radiobutton>
                   <radiobutton visible="'${visible_dock_kids}'">
                    <variable>select_dock_kids</variable>
                    <label>'${msg_dock_kids}' '${comment_dock_kids}'     </label>
                    <default>'${select_dock_kids}'</default>
                   </radiobutton>
                   </frame>

                  <hbox>
                    <button cancel></button>
                    <button can-default="true" has-default="true" use-stock="true" is-focus="true">
                    <label>gtk-ok</label>
                    <action>exit:OK</action>
                    </button>
                  </hbox>

                </vbox>
                </window>'

                MENU_DIALOG_CAIRO_DOCK="$(gtkdialog --center --program=WINDOW_DIALOG_CAIRO_DOCK)"

                eval ${MENU_DIALOG_CAIRO_DOCK}
                echo "MENU_DIALOG_CAIRO_DOCK=${MENU_DIALOG_CAIRO_DOCK}"

                if [ ${EXIT} == "OK" ]
                then

                    if [ ${select_dock_simple} == "true" ] ; then
                        choix=${msg_dock_simple}
                    elif [ ${select_dock_kids} == "true" ] ; then
                        choix=${msg_dock_kids}
                    else
                        choix=${msg_dock_all}
                    fi

                else


                    if ps -A | grep "cairo-dock" ; then

                        echo "Dock actif pas de désactivation de celui-ci"

                    else # Désactivation du dock, si dock non lancé

                        if ps -A | grep "xfce4-session" ; then

                            sed s/"^Dock_XFCE=\"[0-9]*\""/"Dock_XFCE=\"0\""/ ${env_emmabuntus} > ${env_emmabuntus}.tmp

                            cp ${env_emmabuntus}.tmp ${env_emmabuntus}
                            rm ${env_emmabuntus}.tmp

                        fi

                        if ps -A | grep "lxqt-session" ; then

                            sed s/"^Dock_LXQT=\"[0-9]*\""/"Dock_LXQT=\"0\""/ ${env_emmabuntus} > ${env_emmabuntus}.tmp

                            cp ${env_emmabuntus}.tmp ${env_emmabuntus}
                            rm ${env_emmabuntus}.tmp

                        fi

                    fi

                    exit 1

                fi


            fi


            unlink  ~/.config/cairo-dock/current_theme/icons
            unlink  ~/.config/cairo-dock

            echo "$choix"

                if [[ $choix == $msg_dock_all ]]
                then
                    ln -s ${repertoire_cairodock}/cairo-dock-${langue} ~/.config/cairo-dock
                    echo "Dock=All" > ${repertoire_cairodock}/${cairodock_choice_dock}
                    echo "Dock=All" > ${repertoire_cairodock}/${cairodock_choice_dock_old}

                elif [[ $choix == $msg_dock_simple ]]
                then
                    ln -s ${repertoire_cairodock}/cairo-dock-${langue}-simple ~/.config/cairo-dock
                    echo "Dock=Simple" > ${repertoire_cairodock}/${cairodock_choice_dock}
                    echo "Dock=Simple" > ${repertoire_cairodock}/${cairodock_choice_dock_old}

                elif [[ $choix == $msg_dock_kids ]]
                then
                    ln -s ${repertoire_cairodock}/cairo-dock-${langue}-kids ~/.config/cairo-dock
                    echo "Dock=Kids" > ${repertoire_cairodock}/${cairodock_choice_dock}
                    echo "Dock=Kids" > ${repertoire_cairodock}/${cairodock_choice_dock_old}
                else
                    ln -s ${repertoire_cairodock}/cairo-dock-${langue} ~/.config/cairo-dock
                    echo "Dock=All" > ${repertoire_cairodock}/${cairodock_choice_dock}
                    echo "Dock=All" > ${repertoire_cairodock}/${cairodock_choice_dock_old}
                fi

        else

            unlink  ~/.config/cairo-dock/current_theme/icons
            unlink  ~/.config/cairo-dock

            choix=`cat ${repertoire_cairodock}/${cairodock_choice_dock} | grep "Dock=" | cut -d"=" -f 2`
            echo "$choix"


                if [[ $choix == *All* ]]
                then

                    ln -s ${repertoire_cairodock}/cairo-dock-${langue} ~/.config/cairo-dock

                elif [[ $choix == *Simple* ]]
                then
                    if (test -d ${repertoire_cairodock}/cairo-dock-${langue}-simple) ; then
                    ln -s ${repertoire_cairodock}/cairo-dock-${langue}-simple ~/.config/cairo-dock
                    else
                    ln -s ${repertoire_cairodock}/cairo-dock-${langue} ~/.config/cairo-dock
                    fi

                elif [[ $choix == *Kids* ]]
                then
                    if (test -d ${repertoire_cairodock}/cairo-dock-${langue}-kids) ; then
                    ln -s ${repertoire_cairodock}/cairo-dock-${langue}-kids ~/.config/cairo-dock
                    else
                    ln -s ${repertoire_cairodock}/cairo-dock-${langue} ~/.config/cairo-dock
                    fi

                else
                    ln -s ${repertoire_cairodock}/cairo-dock-${langue} ~/.config/cairo-dock

                fi
        fi

    else

        unlink  ~/.config/cairo-dock/current_theme/icons
        unlink  ~/.config/cairo-dock
        ln -s ${repertoire_cairodock}/cairo-dock-${langue} ~/.config/cairo-dock

    fi

# Verification d'un répertoire icons, bloquant la création du lien vers le répertoire icons d'Emmabuntüs
if ! test -L ~/.config/cairo-dock/current_theme/icons
then
    # vérification que le répertoire Cairo-dock est bien une version utilisée pour Emmabuntüs
    if test -f ~/.config/cairo-dock/current_theme/emmabuntus_info.txt
    then
    rm -r ~/.config/cairo-dock/current_theme/icons
    fi

fi

ln -s ${repertoire_cairodock}/cairo-dock-icons ~/.config/cairo-dock/current_theme/icons






# gestion des modes d'affichages du dock
if ( [[ $ecran == ecran_4_3 ]] || [[ $ecran == ecran_5_4 ]] )
then
# Le dock est toujours visible, car l'écran est carré => visibility = mode 1

    if grep -e "^visibility=2$" ~/.config/cairo-dock/current_theme/${cairodock_config}
    then
    # changement de mode du dock
        sed s/^visibility=[0-9]*/visibility=1/ ~/.config/cairo-dock/current_theme/${cairodock_config} > ~/.config/cairo-dock/current_theme/${cairodock_config}.tmp
        cp ~/.config/cairo-dock/current_theme/${cairodock_config}.tmp ~/.config/cairo-dock/current_theme/${cairodock_config}
        rm ~/.config/cairo-dock/current_theme/${cairodock_config}.tmp
    fi

    if grep -e "^visibility=2$" ~/.config/cairo-dock/current_theme/${cairodock_config_simple}
    then
    # changement de mode du dock
        sed s/^visibility=[0-9]*/visibility=1/ ~/.config/cairo-dock/current_theme/${cairodock_config_simple} > ~/.config/cairo-dock/current_theme/${cairodock_config_simple}.tmp
        cp ~/.config/cairo-dock/current_theme/${cairodock_config_simple}.tmp ~/.config/cairo-dock/current_theme/${cairodock_config_simple}
        rm ~/.config/cairo-dock/current_theme/${cairodock_config_simple}.tmp
    fi

elif  ( [[ $ecran == ecran_16_10 ]] || [[ $ecran == ecran_16_9 ]] )
then
# Le dock est toujours visible quand aucune fenêtre n'est maximisée, ou quand la souris approche le bas de l'écran
# car l'écran est allongé => visibility = mode 2

    if grep -e "^visibility=1$" ~/.config/cairo-dock/current_theme/${cairodock_config}
    then
    # changement de mode du dock
        sed s/^visibility=[0-9]*/visibility=2/ ~/.config/cairo-dock/current_theme/${cairodock_config} > ~/.config/cairo-dock/current_theme/${cairodock_config}.tmp
        cp ~/.config/cairo-dock/current_theme/${cairodock_config}.tmp ~/.config/cairo-dock/current_theme/${cairodock_config}
        rm ~/.config/cairo-dock/current_theme/${cairodock_config}.tmp
    fi

    if grep -e "^visibility=1$" ~/.config/cairo-dock/current_theme/${cairodock_config_simple}
    then
    # changement de mode du dock
        sed s/^visibility=[0-9]*/visibility=2/ ~/.config/cairo-dock/current_theme/${cairodock_config_simple} > ~/.config/cairo-dock/current_theme/${cairodock_config_simple}.tmp
        cp ~/.config/cairo-dock/current_theme/${cairodock_config_simple}.tmp ~/.config/cairo-dock/current_theme/${cairodock_config_simple}
        rm ~/.config/cairo-dock/current_theme/${cairodock_config_simple}.tmp
    fi

fi

# Gestion du navigateur Internet pour LXQt
repertoire_cairodock_defaut=~/.config/cairo-dock/current_theme/launchers
browser_xfce=${repertoire_cairodock_defaut}/01firefox-esr.desktop
browser_lxqt=${repertoire_cairodock_defaut}/01falkon.desktop
launcher_browser_xfce="\/usr\/share\/applications\/firefox-esr.desktop"
launcher_browser_lxqt="\/usr\/share\/applications\/org.kde.falkon.desktop"


if ps -A | grep "lxqt-session" ; then

    sed s/"Origin=[^$]*"/"Origin=${launcher_browser_lxqt}"/ ${browser_xfce} > ${browser_xfce}.tmp
    cp ${browser_xfce}.tmp ${browser_xfce}
    rm ${browser_xfce}.tmp

    sed s/"Origin=[^$]*"/"Origin=${launcher_browser_xfce}"/ ${browser_lxqt} > ${browser_lxqt}.tmp
    cp ${browser_lxqt}.tmp ${browser_lxqt}
    rm ${browser_lxqt}.tmp

else

    sed s/"Origin=[^$]*"/"Origin=${launcher_browser_xfce}"/ ${browser_xfce} > ${browser_xfce}.tmp
    cp ${browser_xfce}.tmp ${browser_xfce}
    rm ${browser_xfce}.tmp

    sed s/"Origin=[^$]*"/"Origin=${launcher_browser_lxqt}"/ ${browser_lxqt} > ${browser_lxqt}.tmp
    cp ${browser_lxqt}.tmp ${browser_lxqt}
    rm ${browser_lxqt}.tmp

fi


# Suppression des docks supplémentaires lancés en mode de sauvegarde de session

pkill cairo-dock

if [[ $Lock_Dock == "1" ]]
then
    # Verrouillage des icônes du Dock
    cairo-dock -k -W -c -e xfce &
else
    cairo-dock -W -c -e xfce &
fi






